// Message that come from server is semi-parsed already. But we parse it further
// into basic units that we finally convert to JSX
import createLogger from '../../utils/createLogger';

const log = createLogger('yapreact:MessageParser');

export interface NewLineMessage {
  type: 'nl';
  value: null;
}

export interface TextMessage {
  type: 'text';
  value: string;
}

export interface ImageMessage {
  type: 'image';
  value: string;
}

export interface VideoMessage {
  type: 'video';
  value: string;
}

export interface LinkMessage {
  type: 'link';
  value: {
    name: string;
    href: string;
  };
}

export interface LocationMessage {
  type: 'location';
  value: {
    name: string;
    latitude: number;
    longitude: number;
  };
}

export type ChatMessage =
  | NewLineMessage
  | TextMessage
  | ImageMessage
  | VideoMessage
  | LinkMessage
  | LocationMessage;
type Token = NewLineMessage | TextMessage | LinkMessage;

const tokenizeText = (text: string): Token[] => {
  const breakRegex = /\n|(https?:\/\/[^\s]+)/gi;
  const urlRegex = /(https?:\/\/[^\s]+)/gi;

  // Gotta remove empty strings. Newlines show up as 'undefined'
  const tokens = text.split(breakRegex).filter((tok) => tok !== '');

  return tokens.map((token): Token => {
    if (token === undefined) {
      return { type: 'nl', value: null };
    }

    if (urlRegex.test(token)) {
      return {
        type: 'link',
        value: {
          name: token,
          href: token
        }
      };
    }

    return {
      type: 'text',
      value: token
    };
  });
};

export interface Tokenizable {
  message: string;
  type: 'text' | 'imageText' | 'gifText' | 'locationText' | 'shareLink' | 'videoText' | string;
}
/**
 * Returns array of basic message units.
 * Each array item should become a single message, with its content message units shown in a
 * single message bubble
 */
const tokenize = ({ message, type }: Tokenizable): ChatMessage[] => {
  let messageJson;

  switch (type) {
    case 'text':
      return tokenizeText(message);

    case 'imageText':
      return [
        {
          type: 'image',
          value: message
        }
      ];

    case 'gifText':
      return [
        {
          type: 'image',
          value: message
        }
      ];

    case 'locationText':
      messageJson = JSON.parse(message);
      return [
        {
          type: 'location',
          value: {
            name: messageJson.name,
            latitude: messageJson.latitude,
            longitude: messageJson.longitude
          }
        }
      ];

    case 'shareLink':
      messageJson = JSON.parse(message);

      return messageJson.shareLinkArray.reduce(
        (accum: Array<LinkMessage | ImageMessage>, link: { text?: string; imageLink?: string }) => {
          accum.push({
            type: 'image',
            value: link.imageLink
          });
          accum.push({
            type: 'link',
            value: {
              name: link.text,
              href: link.text
            }
          });
          return accum;
        },
        []
      );

    case 'videoText':
      return [
        {
          type: 'video',
          value: message
        }
      ];

    default:
      log('Received unknown message', message, 'of type', type);
      return [{ type: 'text', value: message }];
  }
};

export default tokenize;
