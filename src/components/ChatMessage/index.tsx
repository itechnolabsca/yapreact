import * as React from 'react';
import * as classnames from 'classnames';

import parseMessage from './messageParser';
import { ChatMessage } from './messageParser';
import { ChatBoxMessage } from '../ChatBox/ducks';

const formatTime = (time: string) => {
  const date = new Date(time);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  let minutes = String(date.getMinutes());
  let hours = date.getHours();
  const ampm = hours >= 12 ? 'pm' : 'am';

  hours %= 12;
  // eslint-disable-next-line
  hours = hours ? hours : 12;
  minutes = parseInt(minutes, 10) < 10 ? `0${minutes}` : minutes;

  const strTime = `${day}/${month}/${year} ${hours}:${minutes} ${ampm}`;

  return strTime;
};

const Message = ({ message }: { message: ChatMessage }) => {
  switch (message.type) {
    case 'text':
      return <span>{message.value}</span>;

    case 'image':
      // eslint-disable-next-line jsx-a11y/img-redundant-alt
      return <img src={message.value} alt="Failed to load Image message" />;

    case 'nl':
      return <br />;

    case 'link':
      return <a href={message.value.href}>{message.value.name}</a>;

    case 'location':
      return (
        <a
          href={`https://www.google.com/maps?q=${message.value.latitude},${
            message.value.longitude
          }`}
          target="_blank"
        >
          <img
            src={`https://maps.google.com/maps/api/staticmap?center=${message.value.latitude},${
              message.value.longitude
            }&zoom=14&size=300x150&&markers=color:red%7Clabel:C%7C${message.value.latitude},${
              message.value.longitude
            }&key=AIzaSyBZLMfSbimJP-GhdZNQ3nwZzyYDl0DQJhA`}
            alt={message.value.name}
          />
        </a>
      );

    case 'video':
      return <video src={message.value} height={190} width={190} controls />;

    default:
      throw new Error('Unknown Message');
  }
};

export interface ChatMessageInput {
  message: {
    type: string;
    message: string;
    deliveredCount: number;
    readCount: number;
    sendTime: any;
  };
  senderId: string;
  receiverId: string;
}

const ChatMessage = ({ message, senderId, receiverId }: ChatMessageInput) => (
  <li className={classnames('message ', receiverId === senderId && 'messageReceived')}>
    {parseMessage(message).map((msg, index) => <Message key={index} message={msg} />)}
    <i
      className={classnames(
        receiverId === senderId ? 'messageReceived' : 'messagesent',
        message.deliveredCount && 'messagedelivered',
        message.readCount && 'messageread'
      )}
    >
      {formatTime(message.sendTime)} {receiverId !== senderId && <span />}
    </i>
  </li>
);

export default ChatMessage;
