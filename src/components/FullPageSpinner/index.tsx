import * as React from 'react';
import './style.scss';

export interface SpinnerInput {
  isVisible: boolean;
}

const Spinner = ({ isVisible }: SpinnerInput) => {
  if (!isVisible) {
    return null;
  }

  return (
    <div className="full-page-spinner__overlay">
      <div className="full-page-spinner__sk-cube-grid">
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube1" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube2" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube3" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube4" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube5" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube6" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube7" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube8" />
        <div className="full-page-spinner__sk-cube full-page-spinner__sk-cube9" />
      </div>
    </div>
  );
};

export default Spinner;
