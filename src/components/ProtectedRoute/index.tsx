import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';

export interface ProtectedRouteProps {
  component: any;
  canProceed: boolean;
  redirectToPath: string;
  exact?: boolean;
  path: string;
  onFail?: () => any;
}

const ProtectedRoute = ({
  component: RouteComponent,
  canProceed,
  redirectToPath,
  onFail,
  ...rest
}: ProtectedRouteProps) => (
  <Route
    {...rest}
    render={(props) => {
      if (canProceed) {
        return <RouteComponent {...props} />;
      }

      if (typeof onFail === 'function') {
        onFail();
      }

      return <Redirect to={{ pathname: redirectToPath, state: { from: props.location } }} />;
    }}
  />
);
/* eslint-enable */

export default ProtectedRoute;
