import * as React from 'react';
import { Component } from 'react';
import createLogger from '../../../utils/createLogger';
import UploadImage from '../index';

const log = createLogger('UploadImageButton');

export interface UploadImageButtonProps {
  formError?: string;
  value?: string;
  label?: string;
  s3Url: string;
  onDoneUpload?: (url: string) => void;
  onChange?: (url: string) => void;
  onError?: (url: string) => void;
  onProgress?: (progress: number) => void;
  supportedFileFormats?: string[];
}

class UploadImageButton extends Component<UploadImageButtonProps> {
  static defaultProps = {
    onDoneUpload: () => ({}),
    onChange: () => ({}),
    onError: () => ({}),
    onProgress: () => ({}),
    supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf']
  };

  loadingEl: HTMLElement;

  state = { error: '' };

  handleProgress = (progress: number) => {
    const { loadingEl } = this;

    if (!loadingEl) {
      log('Loading Element not found', loadingEl); // eslint-disable-line no-console
    } else {
      loadingEl.style.width = `${progress}%`;
    }
    this.props.onProgress(progress);
  };

  handleError = (error: string) => {
    this.setState({ error });
    this.props.onError(error);
  };

  render() {
    const { label, formError, value } = this.props;
    const error = this.state.error || formError;

    return (
      <div className="update-logo form-group">
        <div className="btn btn-primary upload-button">
          <span>{label}</span>
          <UploadImage
            className="form-control"
            onProgress={this.handleProgress}
            onError={this.handleError}
            onDoneUpload={this.props.onDoneUpload}
            onChange={this.props.onChange}
            s3Url={this.props.s3Url}
            supportedFileFormats={this.props.supportedFileFormats}
          />
          <i
            className="upload-button-progress"
            ref={(ref) => {
              this.loadingEl = ref;
            }}
          />
        </div>
        {error && <div className="error text-danger">{error}</div>}
      </div>
    );
  }
}

export default UploadImageButton;
