import * as React from 'react';
import { Component, HtmlHTMLAttributes, FocusEventHandler } from 'react';
import * as AWS from 'aws-sdk';

export interface S3UploaderProps {
  onFocus?: FocusEventHandler<any>;
  onBlur?: FocusEventHandler<any>;
  onDoneUpload?: (url: string) => void;
  onChange?: (url: string) => void;
  onError?: (error: string) => void;
  onProgress?: (progress: number) => void;
  className?: string;
  s3Url: string;
  value?: string;
  supportedFileFormats?: string[];
}

class S3Uploader extends Component<S3UploaderProps> {
  static defaultProps = {
    onFocus: () => ({}),
    onBlur: () => ({}),
    onDoneUpload: () => ({}),
    onChange: () => ({}),
    onProgress: () => ({}),
    supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf']
  };

  fileEl: HTMLInputElement;

  state = { isUploading: false };

  uploadToS3Epic = () => {
    const { fileEl } = this;

    if (fileEl && fileEl.files && fileEl.files.length === 0) {
      return;
    }

    const random = new Date().getTime();
    const file = fileEl.files[0];
    let ext = file.name.split('.').pop();
    ext = ext.toLowerCase();
    const filename = `${random}.${ext}`;

    if (!this.props.supportedFileFormats.includes(ext)) {
      this.setState({
        isUploading: false
      });
      this.props.onError('Unsupported file format.');

      return;
    }

    const s3 = new AWS.S3({
      accessKeyId: 'AKIAIYUDX5CX3IDLLZTA',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    const params = {
      Key: `originals/${filename}`,
      Body: file,
      Bucket: 'pocphotos',
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg'
    };
    const photoUrl = this.props.s3Url + filename;

    const request = s3.putObject(params);

    request.on('build', (req) => {
      req.httpRequest.headers['Access-Control-Allow-Origin'] = '*';
      req.httpRequest.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE';
      req.httpRequest.headers['Access-Control-Allow-Headers'] =
        'Origin, Content-Type, X-Auth-Token';
      req.httpRequest.headers['Content-Length'] = 'stat.size';
      req.httpRequest.headers['Content-Type'] = 'binary/octet-stream';
      req.httpRequest.headers['Content-Transfer-Encoding'] = 'base64';
      req.httpRequest.headers['x-amz-acl'] = 'public-read';
    });

    let int = 1;
    const uploadInterval = setInterval(() => {
      int += 1;
      if (int < 90) {
        this.props.onProgress(int);
        return;
      }
      clearInterval(uploadInterval);
    }, 200);

    this.setState({ isUploading: true });
    request.send((err) => {
      this.props.onProgress(100);
      this.setState({ isUploading: false });
      clearInterval(uploadInterval);

      if (err) {
        this.props.onProgress(0);
        fileEl.value = null; // eslint-disable-line no-param-reassign
        this.props.onError(err.message);
        return;
      }

      if (typeof this.props.onChange === 'function') {
        this.props.onChange(photoUrl);
      }

      if (typeof this.props.onDoneUpload === 'function') {
        this.props.onDoneUpload(photoUrl);
      }
    });
  };

  render() {
    const { value, className } = this.props;
    const { isUploading } = this.state;

    return (
      <input
        type="file"
        className={className}
        disabled={isUploading}
        defaultValue={value}
        onFocus={this.props.onFocus}
        onBlur={this.props.onBlur}
        ref={(ref) => {
          this.fileEl = ref;
        }}
        onChange={() => {
          // simply writing this.uploadToS3Epic isn't working for some reason
          this.uploadToS3Epic();
        }}
      />
    );
  }
}
/* eslint-enable */

export default S3Uploader;
