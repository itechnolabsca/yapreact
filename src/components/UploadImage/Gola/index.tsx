import * as React from 'react';
import { Component } from 'react';
import UploadImage from '../index';
import profileImg from './profile-image.svg';
import createLogger from '../../../utils/createLogger';
import './style.css';

const log = createLogger('UploadImageGola');

export interface UploadImageGolaProps {
  formError?: string;
  value?: string;
  s3Url: string;
  onDoneUpload?: (url: string) => void;
  onChange?: (url: string) => void;
  onError?: (error: string) => void;
  onProgress?: (progress: number) => void;
  supportedFileFormats?: string[];
}

class UploadImageGola extends Component<UploadImageGolaProps> {
  static defaultProps = {
    onDoneUpload: () => ({}),
    onChange: () => ({}),
    onError: () => ({}),
    onProgress: () => ({}),
    supportedFileFormats: ['png', 'jpeg', 'jpg']
  };

  loadingEl: HTMLElement;

  state = { error: '' };

  handleProgress = (progress: number) => {
    const { loadingEl } = this;

    if (!loadingEl) {
      log('Loading Element not found', loadingEl); // eslint-disable-line no-console
    } else {
      loadingEl.style.width = `${progress}%`;
    }

    this.props.onProgress(progress);
  };

  handleError(error: string) {
    this.setState({ error });
    this.props.onError(error);
  }

  render() {
    const { formError, value } = this.props;

    const error = this.state.error || formError;

    return (
      <div className="gola process flex-middle">
        <img className="gola-image" src={profileImg} />
        <UploadImage
          className="gola-file"
          onProgress={this.handleProgress}
          onError={this.handleError}
          onDoneUpload={this.props.onDoneUpload}
          onChange={this.props.onChange}
          s3Url={this.props.s3Url}
          supportedFileFormats={this.props.supportedFileFormats}
        />
        <i
          className="gola-upload-progress"
          ref={(ref) => {
            this.loadingEl = ref;
          }}
        />
        {error && <div className="error text-danger">{error}</div>}
      </div>
    );
  }
}

export default UploadImageGola;
