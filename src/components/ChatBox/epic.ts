import { combineEpics, Epic } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import Socketman from 'socketman';

import { YapAction } from '../../utils/createAction';
import {
  sendMessage,
  receiveNewMessage,
  changeUnsentMessage,
  saveUserMessages,
  removeFromQueue,
  addSentMessage,
  ackSentMsg,
  resetScrollToBottom,
  incUnreadCount,
  scrollToBottom,
  showChatWindow,
  hideChatWindow,
  toggleChatWindow,
  ChatBoxState,
  ChatBoxMessage
} from './ducks';
import { AnyAction } from 'redux';

type UIUser = { _id: string; roomId?: string };
type State = { chatBox: ChatBoxState; ui: { user: UIUser } };

const chatVisibilityEpic: Epic<YapAction<any>, State> = (action$, { getState }) =>
  action$.ofType(toggleChatWindow().type).map(({ payload: user }) => {
    const chatUser = getState().chatBox.user;

    const shallShow = !(chatUser && chatUser.id === user.id);

    return shallShow ? showChatWindow(user) : hideChatWindow(user);
  });

const requestPastMessagesEpic = (socketman: Socketman): Epic<YapAction<any>, State> => (action$) =>
  action$.ofType(showChatWindow().type).map(({ payload: user }) => {
    socketman.emit('getWebMessages', {
      roomId: user.roomId,
      lastMessageId: ''
    });

    return { type: 'REQUESTED_PAST_MESSAGES', user };
  });

const acceptPastMessagesEpic: Epic<AnyAction, State> = (action$) =>
  action$.ofType('@@socketman/PAST_MESSAGES').map(({ payload }) => {
    const messages = payload.sort(
      (m1: ChatBoxMessage, m2: ChatBoxMessage) => m1.sendTime - m2.sendTime
    );

    return saveUserMessages(messages);
  });

const receiveNewMessageEpic: Epic<AnyAction, State> = (action$, { getState }) =>
  action$
    .ofType('@@socketman/NEW_MESSAGE')
    .mergeMap(({ payload }) => {
      if (Array.isArray(payload)) {
        return Observable.from(payload);
      }

      return Observable.of(payload);
    })
    .filter((payload) => {
      const { user } = getState().chatBox;

      return user && payload.senderUserId === user.id;
    })
    .map((payload) => {
      return receiveNewMessage(payload);
    });

const acknowledgeMsgEpic = (socketman: Socketman): Epic<YapAction<any>, State> => (
  action$,
  store
) =>
  action$
    .filter(({ type }) => type === 'SAVE_USER_MESSAGES' || type === 'RECEIVE_NEW_MESSAGE')
    .mergeMap((action) =>
      Observable.from(Array.isArray(action.payload) ? action.payload : [action.payload])
    )
    .filter((message) => message.readCount === 0 || message.deliveredCount === 0)
    .do((msg) => {
      const { user } = store.getState().ui;
      const deliverMsg = {
        type: 'changeMessageStatus_1',
        status: 'deliver',
        messageId: msg._id, // eslint-disable-line no-underscore-dangle
        criteria: 'single',
        roomId: user.roomId
      };

      const readMsg = {
        type: 'changeMessageStatus_1',
        status: 'read',
        messageId: msg._id, // eslint-disable-line no-underscore-dangle
        criteria: 'single',
        roomId: user.roomId
      };

      if (!window.localStorage.getItem('stealthMode')) {
        socketman.emit('changeMessageStatus_1', deliverMsg);
        socketman.emit('changeMessageStatus_1', readMsg);
      }
    })
    .map((message) => ({ type: 'ACK_RECEIVED_MESSAGE', message }));

const sendMessageEpic = (socketman: Socketman): Epic<AnyAction, State> => (action$, { getState }) =>
  action$
    .ofType(sendMessage.start().type)
    .filter(() => {
      const state = getState();
      const message = state.chatBox.unsentMessage;

      return Boolean(message.trim());
    })
    .mergeMap(() => {
      const state = getState();
      const admin = state.ui.user;
      const chatUser = state.chatBox.user;
      const message = state.chatBox.unsentMessage;
      const time = new Date().getTime();

      const socketMsg = {
        roomId: chatUser.roomId,
        message,
        type: 'text',
        senderPhotoId: chatUser.id,
        senderUserId: admin._id, // eslint-disable-line no-underscore-dangle
        sendTime: time,
        localId: String(time),
        uniqueId: time
      };

      socketman.emit('newMessage', socketMsg);

      return Observable.from([changeUnsentMessage(''), addSentMessage(socketMsg)]);
    });

const removeFromQueueEpic = (socketman: Socketman): Epic<AnyAction, State> => (
  action$,
  { getState }
) =>
  action$.ofType(removeFromQueue.start().type).map(() => {
    const state = getState();
    const admin = state.ui.user;
    const chatUser = state.chatBox.user;

    const socketMsg = {
      roomId: chatUser.roomId,
      senderPhotoId: chatUser.id,
      senderUserId: admin._id
    };

    console.log('remove form queue', socketMsg);
    socketman.emit('removeFromQueue', socketMsg);

    return removeFromQueue.success();
  });
const ackSentMsgEpic: Epic<AnyAction, State> = (action$) =>
  action$.ofType('@@socketman/ACK_SENT_MESSAGE').map(({ payload }) => {
    return ackSentMsg(payload[0]);
  });

const startTypingEpic = (socketman: Socketman): Epic<AnyAction, State> => (action$, { getState }) =>
  action$
    .ofType('CHANGE_UNSENT_CHAT_MESSAGE')
    .throttleTime(1000)
    .map(() => {
      const state = getState();
      const chatUser = state.chatBox.user;

      const socketMsg = {
        roomId: chatUser.roomId,
        type: 'typing',
        status: '1'
      };

      socketman.emit('typing', socketMsg);

      return { type: 'SENT_START_TYPING_STATUS' };
    });

const stopTypingEpic = (socketman: Socketman): Epic<AnyAction, State> => (action$, { getState }) =>
  action$
    .ofType('CHANGE_UNSENT_CHAT_MESSAGE')
    .debounceTime(2000)
    .map(() => {
      const state = getState();
      const chatUser = state.chatBox.user;

      const socketMsg = {
        roomId: chatUser.roomId,
        type: 'typing',
        status: '0'
      };

      socketman.emit('typing', socketMsg);

      return { type: 'SENT_STOP_TYPING_STATUS' };
    });

const scrollChatboxEpic: Epic<AnyAction, State> = (action$) => action$.ofType().map(scrollToBottom);

const incUnreadCountEpic: Epic<AnyAction, State> = (action$, { getState }) =>
  action$
    .ofType('RECEIVE_NEW_MESSAGE')
    .filter(() => getState().chatBox.hasScrolledUp)
    .map(incUnreadCount);

const resetScrollToBottomEpic: Epic<AnyAction, State> = (action$) =>
  action$
    .ofType('CHATBOX_SCROLL_TO_BOTTOM')
    .delay(200)
    .map(resetScrollToBottom);

export default (socketman: Socketman) =>
  combineEpics(
    chatVisibilityEpic,
    requestPastMessagesEpic(socketman),
    acceptPastMessagesEpic,
    receiveNewMessageEpic,
    acknowledgeMsgEpic(socketman),
    sendMessageEpic(socketman),
    ackSentMsgEpic,
    startTypingEpic(socketman),
    stopTypingEpic(socketman),
    scrollChatboxEpic,
    incUnreadCountEpic,
    resetScrollToBottomEpic,
    removeFromQueueEpic(socketman)
  );
