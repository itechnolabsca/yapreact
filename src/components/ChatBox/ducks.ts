import { AnyAction } from 'redux';

import { Duck } from '../../utils/createDuck';
import { YapActionCreator } from '../../utils/createAction';
import { YapAsyncAction } from '../../utils/createActionAsync';
import { createAction, createDuck, createReducer } from '../../utils';

export interface ChatBoxMessage {
  localId: string;
  uniqueId: string;
  senderUserId?: string;
  type: string;
  message: string;
  deliveredCount: number;
  readCount: number;
  sendTime: any;
}

export interface ChatBoxState {
  user: any;
  messages: ChatBoxMessage[];
  unsentMessage: string;
  newMessageCount: number;
  shallScrollToBottom: boolean;
  hasScrolledUp: boolean;
}

const sendMessageDuck: Duck<any, ChatBoxState> = createDuck('SEND_CHAT_MESSAGE');
const removeFromQueueDuck: Duck<any, ChatBoxState> = createDuck('REMOVE_FROM_QUEUE');
const initialState: ChatBoxState = {
  user: null,
  messages: [],
  unsentMessage: '',
  newMessageCount: 0,
  shallScrollToBottom: false,
  hasScrolledUp: false
};
const ducks = { messageSendStatus: sendMessageDuck };

export const toggleChatWindow = createAction('TOGGLE_CHAT_WINDOW');
export const changeUnsentMessage = createAction('CHANGE_UNSENT_CHAT_MESSAGE');
export const saveUserMessages = createAction('SAVE_USER_MESSAGES');
export const receiveNewMessage = createAction('RECEIVE_NEW_MESSAGE');
export const addSentMessage = createAction('ADD_SENT_MESSAGE');
export const ackSentMsg = createAction('ACK_SENT_MESSAGE');
export const scrollToBottom = createAction('CHATBOX_SCROLL_TO_BOTTOM');
export const resetScrollToBottom = createAction('CHATBOX_RESET_SCROLL_TO_BOTTOM');
export const scrollUp = createAction('CHATBOX_SCROLL_UP');
export const incUnreadCount = createAction('CHATBOX_INC_UNREAD_COUNT');
export const resetUnreadCount = createAction('CHATBOX_RESET_UNREAD_COUNT');
export const sendMessage = sendMessageDuck.actions;
export const removeFromQueue = removeFromQueueDuck.actions;
export const showChatWindow = createAction('SHOW_CHAT_WINDOW');
export const hideChatWindow = createAction('HIDE_CHAT_WINDOW');

export default createReducer(
  {
    [showChatWindow().type]: (state, { payload: user }) => ({
      ...state,
      user
    }),
    [hideChatWindow().type]: (state) => ({
      ...state,
      user: null
    }),
    CHANGE_UNSENT_CHAT_MESSAGE: (state, { payload: message }) => ({
      ...state,
      unsentMessage: message
    }),
    SAVE_USER_MESSAGES: (state, { payload: messages }) => ({
      ...state,
      messages
    }),
    RECEIVE_NEW_MESSAGE: (state, { payload: message }) => ({
      ...state,
      messages: state.messages.concat([message])
    }),
    ADD_SENT_MESSAGE: (state, { payload: message }) => ({
      ...state,
      messages: state.messages.concat([message])
    }),
    ACK_SENT_MESSAGE: (state, { payload: message }) => {
      const newMessages = state.messages.map((oldMsg) => {
        if (oldMsg.localId && oldMsg.localId === message.localId) {
          return message;
        }

        return oldMsg;
      });

      return {
        ...state,
        messages: newMessages
      };
    },
    CHATBOX_SCROLL_UP: (state) => ({
      ...state,
      hasScrolledUp: true
    }),
    CHATBOX_SCROLL_TO_BOTTOM: (state) => ({
      ...state,
      shallScrollToBottom: true,
      hasScrolledUp: false,
      newMessageCount: 0
    }),
    CHATBOX_INC_UNREAD_COUNT: (state) => ({
      ...state,
      newMessageCount: state.newMessageCount + 1
    }),
    CHATBOX_RESET_UNREAD_COUNT: (state) => ({
      ...state,
      newMessageCount: 0
    }),
    CHATBOX_RESET_SCROLL_TO_BOTTOM: (state) => ({
      ...state,
      shallScrollToBottom: false
    })
  },
  { initialState, ducks }
);
