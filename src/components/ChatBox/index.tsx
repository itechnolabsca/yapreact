import * as React from 'react';
import { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import { YapAction } from '../../utils/createAction';

import {
  toggleChatWindow,
  changeUnsentMessage,
  removeFromQueue,
  sendMessage,
  scrollUp,
  resetUnreadCount,
  ChatBoxMessage,
  ChatBoxState
} from './ducks';
import ChatMessage from '../ChatMessage';

export interface ChatBoxUser {
  id: string;
  name: string;
  thumb?: string;
}

export interface ChatBoxProps {
  user: ChatBoxUser;
  placeholderThumb: string;
  messages: Array<ChatBoxMessage>;
  hasScrolledUp: boolean;
  unsentMessage: string;
  newMessageCount: number;
  shallScrollToBottom: boolean;
  onCloseChat: (u: ChatBoxUser) => (e: any) => void;
  onSendMessage: () => void;
  removeFromQueue: () => void;
  onScrollUp: () => void;
  resetUnreadCount: () => void;
  onChangeMessage: (m: string) => void;
}

class ChatBox extends Component<ChatBoxProps> {
  chatBoxRef: HTMLUListElement | null;
  scrollRef: Element | null;

  componentWillUpdate(newProps: ChatBoxProps) {
    if (
      (newProps.user && !this.props.user) ||
      (!newProps.user && this.props.user) ||
      (this.props.user && newProps.user.id !== this.props.user.id)
    ) {
      this.scrollToBottom();
    }
  }

  componentDidUpdate() {
    if (this.props.shallScrollToBottom) {
      this.scrollToBottom();
    }
  }

  scrollToBottom = () => {
    // do not start an interval if not needed
    const interval = setInterval(() => {
      if (this.chatBoxRef && this.chatBoxRef.lastElementChild) {
        clearInterval(interval);

        this.chatBoxRef.lastElementChild.scrollIntoView();
      }
    }, 100);
  };

  handleChatScroll = () => {
    if (!this.scrollRef) {
      return;
    }

    // Magic number 380
    const scrollDistance = this.scrollRef.scrollHeight - this.scrollRef.scrollTop - 380;

    if (scrollDistance > 100 && !this.props.hasScrolledUp) {
      this.props.onScrollUp();
    }

    if (scrollDistance < 10 && this.props.hasScrolledUp) {
      this.props.resetUnreadCount();
    }
  };

  messagesJsx = () => {
    const { messages, user } = this.props;

    return messages.map((message) => (
      <ChatMessage
        key={message.uniqueId}
        message={message}
        senderId={message.senderUserId}
        receiverId={user.id}
      />
    ));
  };

  unreadBadgeJsx = (count: number) => {
    return (
      <div className="chatScreenNotification" onClick={this.scrollToBottom}>
        <i className="fa fa-angle-down" aria-hidden="true" />
        <span className="badge">{count}</span>
      </div>
    );
  };

  render() {
    const {
      user,
      messages,
      unsentMessage,
      newMessageCount,
      onCloseChat,
      onSendMessage,
      removeFromQueue,
      onChangeMessage,
      placeholderThumb
    } = this.props;

    return (
      user && (
        <div className="chatView">
          <div className="scroll_body">
            <div className="message_title">
              <span className="chatProfilePic">
                <img src={user.thumb || placeholderThumb} alt={user.name} />
              </span>
              {user.name}
              <i className="fa fa-check-circle removeFromQueue" onClick={removeFromQueue}>
                &nbsp;
              </i>
              <i className="fa fa-times close-btn" onClick={onCloseChat(user)}>
                &nbsp;
              </i>
            </div>
            <div
              className="message_body"
              ref={(ref) => (this.scrollRef = ref)}
              onScroll={this.handleChatScroll}
            >
              <ul id="messages" ref={(ref) => (this.chatBoxRef = ref)}>
                {this.messagesJsx()}
              </ul>
            </div>

            <div className="messageTrigger">
              {newMessageCount !== 0 && this.unreadBadgeJsx(newMessageCount)}

              <i
                className="fa fa-paper-plane send"
                onClick={() => {
                  onSendMessage();
                  this.scrollToBottom();
                }}
              >
                &nbsp;
              </i>
              <input
                type="text"
                value={unsentMessage}
                onChange={(event) => onChangeMessage(event.target.value)}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    onSendMessage();
                    this.scrollToBottom();
                  }
                }}
              />
            </div>
          </div>
        </div>
      )
    );
  }
}

type State = { chatBox: ChatBoxState };
const mapStateToProps = ({
  chatBox: { user, messages, unsentMessage, newMessageCount, hasScrolledUp, shallScrollToBottom }
}: State) => ({
  user,
  messages,
  unsentMessage,
  newMessageCount,
  shallScrollToBottom,
  hasScrolledUp
});

const mapDispatchToProps = (dispatch: Dispatch<YapAction<any>>) => ({
  onCloseChat: (user: ChatBoxUser) => () => dispatch(toggleChatWindow(user)),
  removeFromQueue: () => dispatch(removeFromQueue.start()),
  onSendMessage: () => dispatch(sendMessage.start()),
  onChangeMessage: (msg: string) => dispatch(changeUnsentMessage(msg)),
  onScrollUp: () => dispatch(scrollUp()),
  resetUnreadCount: () => dispatch(resetUnreadCount())
});

// HACK: FIXME: ChatBox as any
export default connect(mapStateToProps, mapDispatchToProps)(ChatBox as any);
