import * as React from 'react';
import { connect } from 'react-redux';

import { YapAction } from '../../utils/createAction';
import ChatUserTile from '../ChatUserTile';
import ChatBox from '../ChatBox';
import { toggleChatWindow } from '../ChatBox/ducks';
import { ChatWallUser, ChatWallState } from './ducks';
import { Dispatch } from 'redux';

export interface ChatWallProps {
  users: ChatWallUser[];
  toggleChatWindow: (user: ChatWallUser) => void;
}

const ChatWall = (props: ChatWallProps) => (
  <div>
    <h1 className="dashboard-title hidden-md-up">ChatWall</h1>
    <section className="row text-center customers">
      {props.users.map((user) => (
        <ChatUserTile
          id={user.id}
          key={user.id}
          thumb={user.thumb}
          onlineStatus={user.onlineStatus}
          name={user.name}
          unreadMessagesCount={user.unreadMessagesCount}
          onTileClick={() => props.toggleChatWindow(user)}
        />
      ))}
    </section>

    <ChatBox />
  </div>
);

const mapStateToProps = ({ chatWall: { users } }: { chatWall: ChatWallState }) => ({
  users
});

const mapDispatchToProps = (dispatch: Dispatch<YapAction<ChatWallUser>>) => ({
  toggleChatWindow: (user: ChatWallUser) => dispatch(toggleChatWindow(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatWall);
