import { combineEpics, Epic } from 'redux-observable';
import { AnyAction } from 'redux';
import { ChatBoxState } from '../ChatBox/ducks';

import {
  incUserMsgCount,
  changeUserOnlineStatus,
  resetUnreadMsgCount,
  pushUserToFront
} from './ducks';

type State = { chatBox: ChatBoxState };

const unreadMsgCountEpic: Epic<AnyAction, State> = (action$, { getState }) =>
  action$
    .ofType('@@socketman/NEW_MESSAGE')
    .filter(({ payload }) => {
      const chatUser = getState().chatBox && getState().chatBox.user;
      return !chatUser || (chatUser && chatUser.id !== payload[0].senderUserId);
    })
    .map(({ payload }) =>
      incUserMsgCount({
        id: payload[0].senderUserId
      })
    );

const userStatusEpic: Epic<AnyAction, State> = (action$) =>
  action$.ofType('@@socketman/ONLINE_STATUS').map(({ payload }) =>
    changeUserOnlineStatus({
      id: payload[0].userId,
      onlineStatus: payload[0].status
    })
  );

const resetUnreadMsgCountEpic: Epic<AnyAction, State> = (action$, { getState }) =>
  action$
    .filter(({ type }) => type === 'RECEIVE_NEW_MESSAGE' || type === 'SAVE_USER_MESSAGES')
    .filter(() => getState().chatBox.user)
    .map(() => resetUnreadMsgCount(getState().chatBox.user.id));

const pushUserToFrontEpic: Epic<AnyAction, State> = (action$, { getState }) =>
  action$
    .filter(({ type }) => type === '@@socketman/NEW_MESSAGE' || type === 'START_SEND_CHAT_MESSAGE')
    .map(({ payload }) => {
      let userId;

      if (payload) {
        userId = payload[0].senderUserId;
      } else {
        userId = getState().chatBox.user.id;
      }

      return pushUserToFront(userId);
    });

export default combineEpics(
  unreadMsgCountEpic,
  userStatusEpic,
  resetUnreadMsgCountEpic,
  pushUserToFrontEpic
);
