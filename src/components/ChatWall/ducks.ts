import { AnyAction } from 'redux';
import { createAction, createReducer } from '../../utils';
import { YapAction } from '../../utils/createAction';

export interface ChatWallUser {
  id: string;
  thumb?: string;
  onlineStatus: boolean;
  name: string;
  unreadMessagesCount: number;
}

export interface ChatWallState {
  users: ChatWallUser[];
}

const initialState: ChatWallState = { users: [] };
const ducks = {};

export const changeUserOnlineStatus = createAction('CHANGE_USER_ONLINE_STATUS');
export const incUserMsgCount = createAction('INC_USER_MESSAGE_COUNT');
export const resetUnreadMsgCount = createAction('RESET_UNREAD_MESSAGE_COUNT');
export const pushUserToFront = createAction('PUSH_USER_TO_FRONT');
export const updateUsers = createAction('CHAT_WALL_UPDATE_USERS');

export default createReducer(
  {
    [updateUsers().type]: (state, { payload: users }) => ({
      ...state,
      users
    }),
    [changeUserOnlineStatus().type]: (state, { payload: { id, onlineStatus } }) => {
      const nextUsers = state.users.map((user) => {
        let nextUser = user;

        if (user.id === id) {
          nextUser = {
            ...user,
            onlineStatus
          };
        }

        return nextUser;
      });

      return {
        ...state,
        users: nextUsers
      };
    },
    [incUserMsgCount().type]: (state, { payload }) => {
      const nextUsers = state.users.map((user) => {
        let nextUser = user;

        if (user.id === payload.id) {
          nextUser = {
            ...user,
            unreadMessagesCount: user.unreadMessagesCount + 1
          };
        }

        return nextUser;
      });

      return {
        ...state,
        users: nextUsers
      };
    },
    [resetUnreadMsgCount().type]: (state, { payload: id }) => {
      const nextUsers = state.users.map((user) => {
        let nextUser = user;

        if (user.id === id) {
          nextUser = {
            ...user,
            unreadMessagesCount: 0
          };
        }

        return nextUser;
      });

      return {
        ...state,
        users: nextUsers
      };
    },
    [pushUserToFront().type]: (state, { payload: id }) => {
      const frontUser = state.users.find((user) => user.id === id);
      const nextUsers = state.users.filter((user) => user.id !== id);

      nextUsers.unshift(frontUser);

      return {
        ...state,
        users: nextUsers
      };
    }
  },
  { initialState, ducks }
);
