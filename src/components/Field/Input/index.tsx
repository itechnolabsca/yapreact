import * as React from "react";
import * as classnames from "classnames";
import { MouseEventHandler, ChangeEventHandler } from "react";

export interface InputFieldProps {
  input: {
    onChange: (e: any) => void;
    value?: string;
    [key: string]: any;
  };
  type?: string;
  pattern?: RegExp;
  placeholder?: string;
  label?: string;
  autoComplete?: string;
  wrapperClassName?: string;
  inputClassName?: string;
  errorClassName?: string;
  meta: {
    touched: boolean;
    error: string;
  };
}

const InputField = ({
  input,
  type,
  pattern,
  placeholder,
  label,
  autoComplete,
  wrapperClassName,
  inputClassName,
  errorClassName,
  meta: { touched, error }
}: InputFieldProps) => {
  const onChange: ChangeEventHandler<HTMLInputElement> = e => {
    const val = e.target.value;

    if (val && pattern && !pattern.test(val)) {
      return;
    }

    input.onChange(e);
  };

  const showError = touched && error;
  const inputClass = inputClassName || "form-control";
  const errorClass = errorClassName || "error text-danger";

  return (
    <div
      className={classnames({
        [wrapperClassName]: wrapperClassName,
        "form-group": !wrapperClassName,
        "has-error": showError
      } as any)}
    >
      {label !== null &&
        input.value && <div className="label">{label || placeholder}</div>}

      {type === "checkbox" && (
        <span
          className={classnames("checkbox-before", {
            "checkbox-before-checked": input.checked
          })}
        />
      )}
      <input
        {...input}
        onChange={onChange}
        type={type}
        className={inputClass}
        placeholder={placeholder}
        autoComplete={autoComplete}
        value={input.value}
      />
      {showError && <div className={errorClassName}>{error}</div>}
    </div>
  );
};

export default InputField;
