import * as React from 'react';
import { Component } from 'react';
import * as classnames from 'classnames';
import createLogger from '../../../utils/createLogger';

import UploadImage from '../../UploadImage';

const log = createLogger('UploadImageField');

export interface UploadImageFieldProps {
  formError?: string;
  value?: string;
  label?: string;
  s3Url: string;
  onDoneUpload?: (url: string) => void;
  onChange?: (url: string) => void;
  onError: (url: string) => void;
  onProgress: (progress: number) => void;
  successLabel: string;
  noPreview?: boolean;
  input: {
    onChange: (e: any) => void;
    value?: string;
    [key: string]: any;
  };
  meta: {
    touched: boolean;
    error: string;
    value?: string;
    initial: string;
    warning: string;
  };
  supportedFileFormats?: string[];
  previewableFileFormats?: string[];
}

class UploadImageField extends Component<UploadImageFieldProps> {
  static defaultProps = {
    noPreview: false,
    onDoneUpload: () => ({}),
    onChange: () => ({}),
    onError: () => ({}),
    onProgress: () => ({}),
    supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf'],
    previewableFileFormats: ['png', 'jpeg', 'jpg']
  };

  loadingEl: HTMLElement;

  state = {
    error: '',
    isUploading: false,
    isShowingPreview: false,
    isFocused: false
  };

  setupPreview = (image: string) =>
    this.state.isShowingPreview && (
      <div className="mega-image-viewer">
        <div className="mega-image">
          <img src={image} alt="" />
          <span
            className="fa fa-close"
            onClick={() => {
              this.setState({ isShowingPreview: false });
            }}
          />
        </div>
      </div>
    );

  handleProgress = (progress: number) => {
    const { loadingEl } = this;
    this.setState({ isUploading: true, error: '' });

    if (!loadingEl) {
      log('Loading Element not found', loadingEl); // eslint-disable-line no-console
    } else {
      loadingEl.style.width = `${progress}%`;
    }

    this.props.onProgress(progress);
  };

  handleError = (error: string) => {
    this.setState({ error });
    this.props.onError(error);
  };

  handleDoneUpload = (image: string) => {
    this.setState({
      isUploading: false,
      error: ''
    });

    this.props.onDoneUpload(image);
  };

  render() {
    const {
      input,
      label,
      successLabel,
      noPreview,
      meta: { touched, error: formError, value, initial, warning }
    } = this.props;

    const { error: uploadError, isUploading, isFocused } = this.state;

    const image = input.value || value || initial || '';
    const hasImage = !isUploading && image && image.length;
    const ext =
      hasImage &&
      image
        .split('.')
        .pop()
        .toLowerCase();
    const shallShowPreview = !noPreview && this.props.previewableFileFormats.includes(ext);

    const error = uploadError || formError;
    const hasError = uploadError || (touched && error);

    if (!image && !isUploading && this.loadingEl) {
      this.loadingEl.style.width = '0';
    }

    return (
      <div className="form-group">
        {this.setupPreview(image)}

        <div
          className={classnames({
            'upload-control': true,
            'has-image-preview': hasImage && shallShowPreview,
            'has-error': hasError,
            'is-focused': isFocused
          } as any)}
        >
          {shallShowPreview &&
            hasImage && (
              <img
                className="uploaded-image-preview"
                src={image}
                onClick={() => {
                  this.setState({ isShowingPreview: true });
                }}
                alt="Preview"
              />
            )}

          <UploadImage
            className="form-control"
            onProgress={this.handleProgress}
            onError={this.handleError}
            onDoneUpload={this.handleDoneUpload}
            onChange={input.onChange}
            s3Url={this.props.s3Url}
            value={image}
            onFocus={() => this.setState({ isFocused: true })}
            onBlur={() => this.setState({ isFocused: false })}
            supportedFileFormats={this.props.supportedFileFormats}
          />

          <span>
            <i className="fa fa-cloud-upload">&nbsp;</i>
            {!hasImage && (isUploading ? 'Uploading...' : label || 'Click to Upload')}
            {hasImage && !isUploading && <i>{successLabel || 'Image Uploaded'}</i>}
          </span>
          <i
            className="loading"
            ref={(ref) => {
              this.loadingEl = ref;
            }}
          >
            &nbsp;
          </i>
          <i className="available-formats">
            Supported Formats:{' '}
            {this.props.supportedFileFormats.map((s) => s.toUpperCase()).join(', ')}
          </i>

          {hasError && <div className="error text-danger signup-upload-error">{error}</div>}

          {touched &&
            (warning && (
              <div className="warning text-warning signup-upload-warning">{warning}</div>
            ))}
        </div>
      </div>
    );
  }
}
/* eslint-enable */

export default UploadImageField;
