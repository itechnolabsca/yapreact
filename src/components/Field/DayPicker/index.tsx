import * as React from 'react';
import { ReactElement } from 'react';
import { Options } from 'react-redux';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import './style.css';

export interface DaypickerFieldProps {
  input: {
    value: string;
    onChange: () => void;
    [key: string]: any;
  };
  meta: {
    touched: boolean;
    error: string;
  };
  wrapperClassName?: string;
  inputClassName?: string;
  errorClassName?: string;
  overlayClassName?: string;
  placeholder?: string;
  dayPickerProps?: any;
}

const DayPickerField = ({
  input,
  meta: { touched, error },
  wrapperClassName,
  overlayClassName,
  inputClassName,
  errorClassName,
  placeholder,
  dayPickerProps
}: DaypickerFieldProps) => {
  const wrapperClass = wrapperClassName || 'form-group';
  const inputClass = inputClassName || 'DayPickerInput';
  const overlayClass = overlayClassName || 'DayPickerInput-Overlay';
  const errorClass = errorClassName || 'error text-danger';
  dayPickerProps = dayPickerProps || {};

  return (
    <div className={wrapperClass}>
      <DayPickerInput
        placeholder={placeholder}
        value={input.value}
        onDayChange={input.onChange}
        classNames={{ container: inputClass, overlay: overlayClass }}
        {...dayPickerProps}
      />

      {touched && (error && <div className={errorClass}>{error}</div>)}
    </div>
  );
};

export default DayPickerField;
