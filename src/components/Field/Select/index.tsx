import * as React from 'react';
import { ReactElement } from 'react';
import { Options } from 'react-redux';

export interface SelectFieldProps {
  input: {
    value?: string;
    [key: string]: any;
  };
  type?: string;
  pattern?: RegExp;
  placeholder?: string;
  label?: string;
  meta: {
    touched: boolean;
    error: string;
  };
  children?: Array<ReactElement<Options>>;
  wrapperClassName?: string;
  inputClassName?: string;
  errorClassName?: string;
}

const SelectField = ({
  input,
  label,
  type,
  meta: { touched, error },
  children,
  placeholder,
  wrapperClassName,
  inputClassName,
  errorClassName
}: SelectFieldProps) => {
  const wrapperClass = wrapperClassName || 'form-group';
  const inputClass = inputClassName || 'form-control';
  const errorClass = errorClassName || 'error text-danger';

  return (
    <div className={wrapperClass}>
      {input.value && <div className="label">{label || placeholder}</div>}

      <select className={inputClass} {...input}>
        {children}
      </select>
      {touched && (error && <div className={errorClass}>{error}</div>)}
    </div>
  );
};

export default SelectField;
