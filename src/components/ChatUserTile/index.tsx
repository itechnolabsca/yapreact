import * as React from 'react';
import { MouseEvent } from 'react';
import * as classnames from 'classnames';

export interface ChatTileInput {
  id: string;
  key?: string;
  thumb: string;
  onlineStatus: boolean;
  name: string;
  unreadMessagesCount: number;
  onTileClick: (e: MouseEvent<HTMLElement>) => void;
}

const ChatUserTile = ({
  id,
  thumb,
  onlineStatus,
  name,
  unreadMessagesCount,
  onTileClick: onTileClick
}: ChatTileInput) => (
  <div className="col-lg-2 col-md-2 col-sm-3 thumb" id={id} onClick={onTileClick}>
    <span className={classnames('status', onlineStatus ? 'online' : 'offline')}>&nbsp;</span>
    {unreadMessagesCount > 0 && <span className="notification">{unreadMessagesCount}</span>}

    <div className="thumbnail">
      <img className="img-responsive" src={thumb} alt={name} />
    </div>
  </div>
);

export default ChatUserTile;
