import { Epic } from 'redux-observable';
import { YapAction } from '../../utils/createAction';
import { addNotification, onCloseNotiBoi, NotiBoiState } from './ducks';

const autoCloseEpic: Epic<YapAction<any>, NotiBoiState> = (action$) =>
  action$
    .ofType(addNotification().type)
    .delay(10 * 1000)
    .map(({ payload }) => onCloseNotiBoi(payload));

export default autoCloseEpic;
