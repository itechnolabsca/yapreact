import * as React from 'react';
import { Dispatch } from 'redux';

import { YapAction } from '../../utils/createAction';
import { NotiBoiState, NotificationI, onHandleNotiboi, onCloseNotiBoi } from './ducks';
import './style.scss';

export interface NotificationProps {
  notification: NotificationI;
  onAction: (n: NotificationI) => void;
  onClose: (n: NotificationI) => void;
}

const Notification = ({ onClose, onAction, notification: n }: NotificationProps) => {
  const hasAction = Boolean(n.actionName);
  return (
    <div className={`notiboi__notification notiboi__notification-${n.type}`}>
      <span className="notiboi__action notiboi__action--close" onClick={() => onClose(n)}>
        Close
      </span>
      {hasAction && (
        <span className="notiboi__action notiboi__action--primary" onClick={() => onAction(n)}>
          {n.actionName}
        </span>
      )}
      <div className="notiboi__notification__title">{n.title}</div>
      <div className="notiboi__notification__body">{n.body}</div>
    </div>
  );
};

export interface NotificationRootProps {
  notifications: { [id: string]: NotificationI };
  dispatch: Dispatch<YapAction<NotificationI>>;
}
const NotificationRoot = ({ notifications, dispatch }: NotificationRootProps) => (
  <div className="notiboi-root__container">
    {Object.keys(notifications).map((k) => (
      <Notification
        key={notifications[k].id}
        onAction={(n) => dispatch(onHandleNotiboi(n))}
        onClose={(n) => dispatch(onCloseNotiBoi(n))}
        notification={notifications[k]}
      />
    ))}
  </div>
);

export default NotificationRoot;
