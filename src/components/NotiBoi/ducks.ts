import { createReducer, createAction, guid } from '../../utils';
import { YapAction, YapActionCreator } from '../../utils/createAction';
import { Reducer } from 'redux';

export interface NotificationI {
  id: string;
  title: string;
  type: 'info' | 'error' | 'success';
  body?: string;
  actionName?: string;
  meta: any;
}

export interface NotiBoiState {
  notifications: {
    [id: string]: NotificationI;
  };
}

const initialState: NotiBoiState = {
  notifications: {}
};

export const addNotification: YapActionCreator<NotificationI> = createAction(
  '@@notiboi/ADD_NOTIFICATION'
);
export const onHandleNotiboi: YapActionCreator<NotificationI> = createAction('@@notiboi/HANDLE');
export const onCloseNotiBoi: YapActionCreator<NotificationI> = createAction('@@notiboi/CLOSE');

export interface NotifyOptions {
  type?: 'info' | 'error' | 'success';
  body?: string;
  actionName?: string;
  meta?: any;
}
export const notify = (title: string, options?: NotifyOptions) =>
  addNotification({
    id: guid(),
    title,
    type: (options && options.type) || 'info',
    body: (options && options.body) || '',
    actionName: (options && options.actionName) || '',
    meta: (options && options.meta) || {}
  });

export default createReducer(
  {
    [addNotification().type]: (state, { payload }: YapAction<NotificationI>) => ({
      ...state,
      notifications: {
        ...state.notifications,
        [payload.id]: payload
      }
    }),
    [onCloseNotiBoi().type]: (state, { payload }: YapAction<NotificationI>) => {
      const notifications = { ...state.notifications };
      delete notifications[payload.id];

      return {
        ...state,
        notifications
      };
    }
  },
  { initialState }
);
