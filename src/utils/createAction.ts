import { Action } from 'redux';

export interface YapAction<T> {
  type: string;
  payload?: T;
}

export type YapActionCreator<T> = (payload?: T) => YapAction<T>;

export default <T>(type: string): YapActionCreator<T> => (payload?: T) => ({
  type,
  payload
});
