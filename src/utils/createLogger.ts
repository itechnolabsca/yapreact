import * as debug from 'debug';

const logger = (name: string) => debug(`YAP:${name}`);

export default logger;
