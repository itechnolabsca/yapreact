import createAction from './createAction';
import { YapAction } from './createAction';

export interface YapAsyncAction {
  start: <T>(payload?: T) => YapAction<T>;
  success: <T>(payload?: T) => YapAction<T>;
  fail: <T>(payload?: T) => YapAction<T>;
}

export default (type: string): YapAsyncAction => ({
  start: createAction(`START_${type}`),
  success: createAction(`SUCCESS_${type}`),
  fail: createAction(`FAIL_${type}`)
});
