export default (href: string) => {
  const link = document.createElement('a');

  link.href = href;
  link.download = href;
  link.target = '_blank';
  link.click();
};
