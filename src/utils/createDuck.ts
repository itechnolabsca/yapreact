import { Reducer } from 'redux';
import createActionAsync from './createActionAsync';
import { YapAsyncAction } from './createActionAsync';
import { ReducerMap } from './createReducer';

export interface DuckOptions {
  clearOnStart?: boolean;
  clearOnError?: boolean;
  storeNothing?: boolean;
}

export interface DuckStateNode<T> {
  isBusy: boolean;
  error: string;
  data: T | any[];
}

export interface Duck<T, S = {}> {
  reducerMap: (type: string) => ReducerMap<S>;
  actions: YapAsyncAction;
  initialState: DuckStateNode<T>;
}

export default <T, S>(name: string, initialData?: T | any[], options?: DuckOptions): Duck<T, S> => {
  const data = initialData === undefined ? [] : initialData;
  options = options || {};
  const storeNothing = data === null || options.storeNothing === true;
  const initialState: DuckStateNode<T> = {
    isBusy: false,
    data,
    error: ''
  };
  const actions = createActionAsync(name.toUpperCase());

  const reducerMap = (stateKey: string): ReducerMap<S> => ({
    [actions.start().type]: (state) =>
      Object.assign({}, state, {
        [stateKey]: {
          isBusy: true,
          error: '',
          data: storeNothing
            ? initialState.data
            : options.clearOnStart === false ? state[stateKey].data : data
        }
      }),
    [actions.fail().type]: (state, { payload }) =>
      Object.assign({}, state, {
        [stateKey]: {
          isBusy: false,
          error: payload,
          data: storeNothing
            ? initialState.data
            : options.clearOnError === false ? state[stateKey].data : data
        }
      }),
    [actions.success().type]: (state, { payload }) =>
      Object.assign({}, state, {
        [stateKey]: {
          error: '',
          isBusy: false,
          data: storeNothing ? initialState.data : payload
        }
      })
  });

  return {
    actions,
    initialState,
    reducerMap
  };
};
