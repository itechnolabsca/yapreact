import { YapAction, YapActionCreator } from '../../utils/createAction';
import { Reducer } from 'redux';
export interface NotificationI {
    id: string;
    title: string;
    type: 'info' | 'error' | 'success';
    body?: string;
    actionName?: string;
    meta: any;
}
export interface NotiBoiState {
    notifications: {
        [id: string]: NotificationI;
    };
}
export declare const addNotification: YapActionCreator<NotificationI>;
export declare const onHandleNotiboi: YapActionCreator<NotificationI>;
export declare const onCloseNotiBoi: YapActionCreator<NotificationI>;
export interface NotifyOptions {
    type?: 'info' | 'error' | 'success';
    body?: string;
    actionName?: string;
    meta?: any;
}
export declare const notify: (title: string, options?: NotifyOptions) => YapAction<NotificationI>;
declare const _default: Reducer<NotiBoiState>;
export default _default;
