/// <reference types="react" />
import { Dispatch } from 'redux';
import { YapAction } from '../../utils/createAction';
import { NotificationI } from './ducks';
import './style.scss';
export interface NotificationProps {
    notification: NotificationI;
    onAction: (n: NotificationI) => void;
    onClose: (n: NotificationI) => void;
}
export interface NotificationRootProps {
    notifications: {
        [id: string]: NotificationI;
    };
    dispatch: Dispatch<YapAction<NotificationI>>;
}
declare const NotificationRoot: ({ notifications, dispatch }: NotificationRootProps) => JSX.Element;
export default NotificationRoot;
