"use strict";
exports.__esModule = true;
var ducks_1 = require("./ducks");
var autoCloseEpic = function (action$) {
    return action$
        .ofType(ducks_1.addNotification().type)
        .delay(10 * 1000)
        .map(function (_a) {
        var payload = _a.payload;
        return ducks_1.onCloseNotiBoi(payload);
    });
};
exports["default"] = autoCloseEpic;
//# sourceMappingURL=epic.js.map