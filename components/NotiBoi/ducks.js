"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var _a;
var utils_1 = require("../../utils");
var initialState = {
    notifications: {}
};
exports.addNotification = utils_1.createAction('@@notiboi/ADD_NOTIFICATION');
exports.onHandleNotiboi = utils_1.createAction('@@notiboi/HANDLE');
exports.onCloseNotiBoi = utils_1.createAction('@@notiboi/CLOSE');
exports.notify = function (title, options) {
    return exports.addNotification({
        id: utils_1.guid(),
        title: title,
        type: (options && options.type) || 'info',
        body: (options && options.body) || '',
        actionName: (options && options.actionName) || '',
        meta: (options && options.meta) || {}
    });
};
exports["default"] = utils_1.createReducer((_a = {},
    _a[exports.addNotification().type] = function (state, _a) {
        var payload = _a.payload;
        var _b;
        return (__assign({}, state, { notifications: __assign({}, state.notifications, (_b = {}, _b[payload.id] = payload, _b)) }));
    },
    _a[exports.onCloseNotiBoi().type] = function (state, _a) {
        var payload = _a.payload;
        var notifications = __assign({}, state.notifications);
        delete notifications[payload.id];
        return __assign({}, state, { notifications: notifications });
    },
    _a), { initialState: initialState });
//# sourceMappingURL=ducks.js.map