import { Epic } from 'redux-observable';
import { YapAction } from '../../utils/createAction';
import { NotiBoiState } from './ducks';
declare const autoCloseEpic: Epic<YapAction<any>, NotiBoiState>;
export default autoCloseEpic;
