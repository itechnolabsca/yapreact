"use strict";
exports.__esModule = true;
var React = require("react");
var ducks_1 = require("./ducks");
require("./style.scss");
var Notification = function (_a) {
    var onClose = _a.onClose, onAction = _a.onAction, n = _a.notification;
    var hasAction = Boolean(n.actionName);
    return (React.createElement("div", { className: "notiboi__notification notiboi__notification-" + n.type },
        React.createElement("span", { className: "notiboi__action notiboi__action--close", onClick: function () { return onClose(n); } }, "Close"),
        hasAction && (React.createElement("span", { className: "notiboi__action notiboi__action--primary", onClick: function () { return onAction(n); } }, n.actionName)),
        React.createElement("div", { className: "notiboi__notification__title" }, n.title),
        React.createElement("div", { className: "notiboi__notification__body" }, n.body)));
};
var NotificationRoot = function (_a) {
    var notifications = _a.notifications, dispatch = _a.dispatch;
    return (React.createElement("div", { className: "notiboi-root__container" }, Object.keys(notifications).map(function (k) { return (React.createElement(Notification, { key: notifications[k].id, onAction: function (n) { return dispatch(ducks_1.onHandleNotiboi(n)); }, onClose: function (n) { return dispatch(ducks_1.onCloseNotiBoi(n)); }, notification: notifications[k] })); })));
};
exports["default"] = NotificationRoot;
//# sourceMappingURL=index.js.map