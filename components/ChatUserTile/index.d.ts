import { MouseEvent } from 'react';
export interface ChatTileInput {
    id: string;
    key?: string;
    thumb: string;
    onlineStatus: boolean;
    name: string;
    unreadMessagesCount: number;
    onTileClick: (e: MouseEvent<HTMLElement>) => void;
}
declare const ChatUserTile: ({ id, thumb, onlineStatus, name, unreadMessagesCount, onTileClick: onTileClick }: ChatTileInput) => JSX.Element;
export default ChatUserTile;
