"use strict";
exports.__esModule = true;
var React = require("react");
var classnames = require("classnames");
var ChatUserTile = function (_a) {
    var id = _a.id, thumb = _a.thumb, onlineStatus = _a.onlineStatus, name = _a.name, unreadMessagesCount = _a.unreadMessagesCount, onTileClick = _a.onTileClick;
    return (React.createElement("div", { className: "col-lg-2 col-md-2 col-sm-3 thumb", id: id, onClick: onTileClick },
        React.createElement("span", { className: classnames('status', onlineStatus ? 'online' : 'offline') }, "\u00A0"),
        unreadMessagesCount > 0 && React.createElement("span", { className: "notification" }, unreadMessagesCount),
        React.createElement("div", { className: "thumbnail" },
            React.createElement("img", { className: "img-responsive", src: thumb, alt: name }))));
};
exports["default"] = ChatUserTile;
//# sourceMappingURL=index.js.map