"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_1 = require("react");
var index_1 = require("../index");
var profile_image_svg_1 = require("./profile-image.svg");
var createLogger_1 = require("../../../utils/createLogger");
require("./style.css");
var log = createLogger_1["default"]('UploadImageGola');
var UploadImageGola = /** @class */ (function (_super) {
    __extends(UploadImageGola, _super);
    function UploadImageGola() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { error: '' };
        _this.handleProgress = function (progress) {
            var loadingEl = _this.loadingEl;
            if (!loadingEl) {
                log('Loading Element not found', loadingEl); // eslint-disable-line no-console
            }
            else {
                loadingEl.style.width = progress + "%";
            }
            _this.props.onProgress(progress);
        };
        return _this;
    }
    UploadImageGola.prototype.handleError = function (error) {
        this.setState({ error: error });
        this.props.onError(error);
    };
    UploadImageGola.prototype.render = function () {
        var _this = this;
        var _a = this.props, formError = _a.formError, value = _a.value;
        var error = this.state.error || formError;
        return (React.createElement("div", { className: "gola process flex-middle" },
            React.createElement("img", { className: "gola-image", src: profile_image_svg_1["default"] }),
            React.createElement(index_1["default"], { className: "gola-file", onProgress: this.handleProgress, onError: this.handleError, onDoneUpload: this.props.onDoneUpload, onChange: this.props.onChange, s3Url: this.props.s3Url, supportedFileFormats: this.props.supportedFileFormats }),
            React.createElement("i", { className: "gola-upload-progress", ref: function (ref) {
                    _this.loadingEl = ref;
                } }),
            error && React.createElement("div", { className: "error text-danger" }, error)));
    };
    UploadImageGola.defaultProps = {
        onDoneUpload: function () { return ({}); },
        onChange: function () { return ({}); },
        onError: function () { return ({}); },
        onProgress: function () { return ({}); },
        supportedFileFormats: ['png', 'jpeg', 'jpg']
    };
    return UploadImageGola;
}(react_1.Component));
exports["default"] = UploadImageGola;
//# sourceMappingURL=index.js.map