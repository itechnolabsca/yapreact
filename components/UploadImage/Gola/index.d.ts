import { Component } from 'react';
import './style.css';
export interface UploadImageGolaProps {
    formError?: string;
    value?: string;
    s3Url: string;
    onDoneUpload?: (url: string) => void;
    onChange?: (url: string) => void;
    onError?: (error: string) => void;
    onProgress?: (progress: number) => void;
    supportedFileFormats?: string[];
}
declare class UploadImageGola extends Component<UploadImageGolaProps> {
    static defaultProps: {
        onDoneUpload: () => {};
        onChange: () => {};
        onError: () => {};
        onProgress: () => {};
        supportedFileFormats: string[];
    };
    loadingEl: HTMLElement;
    state: {
        error: string;
    };
    handleProgress: (progress: number) => void;
    handleError(error: string): void;
    render(): JSX.Element;
}
export default UploadImageGola;
