import { Component, FocusEventHandler } from 'react';
export interface S3UploaderProps {
    onFocus?: FocusEventHandler<any>;
    onBlur?: FocusEventHandler<any>;
    onDoneUpload?: (url: string) => void;
    onChange?: (url: string) => void;
    onError?: (error: string) => void;
    onProgress?: (progress: number) => void;
    className?: string;
    s3Url: string;
    value?: string;
    supportedFileFormats?: string[];
}
declare class S3Uploader extends Component<S3UploaderProps> {
    static defaultProps: {
        onFocus: () => {};
        onBlur: () => {};
        onDoneUpload: () => {};
        onChange: () => {};
        onProgress: () => {};
        supportedFileFormats: string[];
    };
    fileEl: HTMLInputElement;
    state: {
        isUploading: boolean;
    };
    uploadToS3Epic: () => void;
    render(): JSX.Element;
}
export default S3Uploader;
