"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_1 = require("react");
var AWS = require("aws-sdk");
var S3Uploader = /** @class */ (function (_super) {
    __extends(S3Uploader, _super);
    function S3Uploader() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { isUploading: false };
        _this.uploadToS3Epic = function () {
            var fileEl = _this.fileEl;
            if (fileEl && fileEl.files && fileEl.files.length === 0) {
                return;
            }
            var random = new Date().getTime();
            var file = fileEl.files[0];
            var ext = file.name.split('.').pop();
            ext = ext.toLowerCase();
            var filename = random + "." + ext;
            if (!_this.props.supportedFileFormats.includes(ext)) {
                _this.setState({
                    isUploading: false
                });
                _this.props.onError('Unsupported file format.');
                return;
            }
            var s3 = new AWS.S3({
                accessKeyId: 'AKIAIYUDX5CX3IDLLZTA',
                secretAccessKey: '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',
                region: 'us-west-2'
            });
            var params = {
                Key: "originals/" + filename,
                Body: file,
                Bucket: 'pocphotos',
                ContentEncoding: 'base64',
                ContentType: 'image/jpeg'
            };
            var photoUrl = _this.props.s3Url + filename;
            var request = s3.putObject(params);
            request.on('build', function (req) {
                req.httpRequest.headers['Access-Control-Allow-Origin'] = '*';
                req.httpRequest.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE';
                req.httpRequest.headers['Access-Control-Allow-Headers'] =
                    'Origin, Content-Type, X-Auth-Token';
                req.httpRequest.headers['Content-Length'] = 'stat.size';
                req.httpRequest.headers['Content-Type'] = 'binary/octet-stream';
                req.httpRequest.headers['Content-Transfer-Encoding'] = 'base64';
                req.httpRequest.headers['x-amz-acl'] = 'public-read';
            });
            var int = 1;
            var uploadInterval = setInterval(function () {
                int += 1;
                if (int < 90) {
                    _this.props.onProgress(int);
                    return;
                }
                clearInterval(uploadInterval);
            }, 200);
            _this.setState({ isUploading: true });
            request.send(function (err) {
                _this.props.onProgress(100);
                _this.setState({ isUploading: false });
                clearInterval(uploadInterval);
                if (err) {
                    _this.props.onProgress(0);
                    fileEl.value = null; // eslint-disable-line no-param-reassign
                    _this.props.onError(err.message);
                    return;
                }
                if (typeof _this.props.onChange === 'function') {
                    _this.props.onChange(photoUrl);
                }
                if (typeof _this.props.onDoneUpload === 'function') {
                    _this.props.onDoneUpload(photoUrl);
                }
            });
        };
        return _this;
    }
    S3Uploader.prototype.render = function () {
        var _this = this;
        var _a = this.props, value = _a.value, className = _a.className;
        var isUploading = this.state.isUploading;
        return (React.createElement("input", { type: "file", className: className, disabled: isUploading, defaultValue: value, onFocus: this.props.onFocus, onBlur: this.props.onBlur, ref: function (ref) {
                _this.fileEl = ref;
            }, onChange: function () {
                // simply writing this.uploadToS3Epic isn't working for some reason
                _this.uploadToS3Epic();
            } }));
    };
    S3Uploader.defaultProps = {
        onFocus: function () { return ({}); },
        onBlur: function () { return ({}); },
        onDoneUpload: function () { return ({}); },
        onChange: function () { return ({}); },
        onProgress: function () { return ({}); },
        supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf']
    };
    return S3Uploader;
}(react_1.Component));
/* eslint-enable */
exports["default"] = S3Uploader;
//# sourceMappingURL=index.js.map