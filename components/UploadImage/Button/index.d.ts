import { Component } from 'react';
export interface UploadImageButtonProps {
    formError?: string;
    value?: string;
    label?: string;
    s3Url: string;
    onDoneUpload?: (url: string) => void;
    onChange?: (url: string) => void;
    onError?: (url: string) => void;
    onProgress?: (progress: number) => void;
    supportedFileFormats?: string[];
}
declare class UploadImageButton extends Component<UploadImageButtonProps> {
    static defaultProps: {
        onDoneUpload: () => {};
        onChange: () => {};
        onError: () => {};
        onProgress: () => {};
        supportedFileFormats: string[];
    };
    loadingEl: HTMLElement;
    state: {
        error: string;
    };
    handleProgress: (progress: number) => void;
    handleError: (error: string) => void;
    render(): JSX.Element;
}
export default UploadImageButton;
