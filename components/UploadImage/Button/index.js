"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_1 = require("react");
var createLogger_1 = require("../../../utils/createLogger");
var index_1 = require("../index");
var log = createLogger_1["default"]('UploadImageButton');
var UploadImageButton = /** @class */ (function (_super) {
    __extends(UploadImageButton, _super);
    function UploadImageButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { error: '' };
        _this.handleProgress = function (progress) {
            var loadingEl = _this.loadingEl;
            if (!loadingEl) {
                log('Loading Element not found', loadingEl); // eslint-disable-line no-console
            }
            else {
                loadingEl.style.width = progress + "%";
            }
            _this.props.onProgress(progress);
        };
        _this.handleError = function (error) {
            _this.setState({ error: error });
            _this.props.onError(error);
        };
        return _this;
    }
    UploadImageButton.prototype.render = function () {
        var _this = this;
        var _a = this.props, label = _a.label, formError = _a.formError, value = _a.value;
        var error = this.state.error || formError;
        return (React.createElement("div", { className: "update-logo form-group" },
            React.createElement("div", { className: "btn btn-primary upload-button" },
                React.createElement("span", null, label),
                React.createElement(index_1["default"], { className: "form-control", onProgress: this.handleProgress, onError: this.handleError, onDoneUpload: this.props.onDoneUpload, onChange: this.props.onChange, s3Url: this.props.s3Url, supportedFileFormats: this.props.supportedFileFormats }),
                React.createElement("i", { className: "upload-button-progress", ref: function (ref) {
                        _this.loadingEl = ref;
                    } })),
            error && React.createElement("div", { className: "error text-danger" }, error)));
    };
    UploadImageButton.defaultProps = {
        onDoneUpload: function () { return ({}); },
        onChange: function () { return ({}); },
        onError: function () { return ({}); },
        onProgress: function () { return ({}); },
        supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf']
    };
    return UploadImageButton;
}(react_1.Component));
exports["default"] = UploadImageButton;
//# sourceMappingURL=index.js.map