/// <reference types="react" />
import './style.scss';
export interface SpinnerInput {
    isVisible: boolean;
}
declare const Spinner: ({ isVisible }: SpinnerInput) => JSX.Element;
export default Spinner;
