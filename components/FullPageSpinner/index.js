"use strict";
exports.__esModule = true;
var React = require("react");
require("./style.scss");
var Spinner = function (_a) {
    var isVisible = _a.isVisible;
    if (!isVisible) {
        return null;
    }
    return (React.createElement("div", { className: "full-page-spinner__overlay" },
        React.createElement("div", { className: "full-page-spinner__sk-cube-grid" },
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube1" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube2" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube3" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube4" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube5" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube6" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube7" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube8" }),
            React.createElement("div", { className: "full-page-spinner__sk-cube full-page-spinner__sk-cube9" }))));
};
exports["default"] = Spinner;
//# sourceMappingURL=index.js.map