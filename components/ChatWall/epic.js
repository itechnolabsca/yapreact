"use strict";
exports.__esModule = true;
var redux_observable_1 = require("redux-observable");
var ducks_1 = require("./ducks");
var unreadMsgCountEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType('@@socketman/NEW_MESSAGE')
        .filter(function (_a) {
        var payload = _a.payload;
        var chatUser = getState().chatBox && getState().chatBox.user;
        return !chatUser || (chatUser && chatUser.id !== payload[0].senderUserId);
    })
        .map(function (_a) {
        var payload = _a.payload;
        return ducks_1.incUserMsgCount({
            id: payload[0].senderUserId
        });
    });
};
var userStatusEpic = function (action$) {
    return action$.ofType('@@socketman/ONLINE_STATUS').map(function (_a) {
        var payload = _a.payload;
        return ducks_1.changeUserOnlineStatus({
            id: payload[0].userId,
            onlineStatus: payload[0].status
        });
    });
};
var resetUnreadMsgCountEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$
        .filter(function (_a) {
        var type = _a.type;
        return type === 'RECEIVE_NEW_MESSAGE' || type === 'SAVE_USER_MESSAGES';
    })
        .filter(function () { return getState().chatBox.user; })
        .map(function () { return ducks_1.resetUnreadMsgCount(getState().chatBox.user.id); });
};
var pushUserToFrontEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$
        .filter(function (_a) {
        var type = _a.type;
        return type === '@@socketman/NEW_MESSAGE' || type === 'START_SEND_CHAT_MESSAGE';
    })
        .map(function (_a) {
        var payload = _a.payload;
        var userId;
        if (payload) {
            userId = payload[0].senderUserId;
        }
        else {
            userId = getState().chatBox.user.id;
        }
        return ducks_1.pushUserToFront(userId);
    });
};
exports["default"] = redux_observable_1.combineEpics(unreadMsgCountEpic, userStatusEpic, resetUnreadMsgCountEpic, pushUserToFrontEpic);
//# sourceMappingURL=epic.js.map