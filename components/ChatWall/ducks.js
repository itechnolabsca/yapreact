"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var _a;
var utils_1 = require("../../utils");
var initialState = { users: [] };
var ducks = {};
exports.changeUserOnlineStatus = utils_1.createAction('CHANGE_USER_ONLINE_STATUS');
exports.incUserMsgCount = utils_1.createAction('INC_USER_MESSAGE_COUNT');
exports.resetUnreadMsgCount = utils_1.createAction('RESET_UNREAD_MESSAGE_COUNT');
exports.pushUserToFront = utils_1.createAction('PUSH_USER_TO_FRONT');
exports.updateUsers = utils_1.createAction('CHAT_WALL_UPDATE_USERS');
exports["default"] = utils_1.createReducer((_a = {},
    _a[exports.updateUsers().type] = function (state, _a) {
        var users = _a.payload;
        return (__assign({}, state, { users: users }));
    },
    _a[exports.changeUserOnlineStatus().type] = function (state, _a) {
        var _b = _a.payload, id = _b.id, onlineStatus = _b.onlineStatus;
        var nextUsers = state.users.map(function (user) {
            var nextUser = user;
            if (user.id === id) {
                nextUser = __assign({}, user, { onlineStatus: onlineStatus });
            }
            return nextUser;
        });
        return __assign({}, state, { users: nextUsers });
    },
    _a[exports.incUserMsgCount().type] = function (state, _a) {
        var payload = _a.payload;
        var nextUsers = state.users.map(function (user) {
            var nextUser = user;
            if (user.id === payload.id) {
                nextUser = __assign({}, user, { unreadMessagesCount: user.unreadMessagesCount + 1 });
            }
            return nextUser;
        });
        return __assign({}, state, { users: nextUsers });
    },
    _a[exports.resetUnreadMsgCount().type] = function (state, _a) {
        var id = _a.payload;
        var nextUsers = state.users.map(function (user) {
            var nextUser = user;
            if (user.id === id) {
                nextUser = __assign({}, user, { unreadMessagesCount: 0 });
            }
            return nextUser;
        });
        return __assign({}, state, { users: nextUsers });
    },
    _a[exports.pushUserToFront().type] = function (state, _a) {
        var id = _a.payload;
        var frontUser = state.users.find(function (user) { return user.id === id; });
        var nextUsers = state.users.filter(function (user) { return user.id !== id; });
        nextUsers.unshift(frontUser);
        return __assign({}, state, { users: nextUsers });
    },
    _a), { initialState: initialState, ducks: ducks });
//# sourceMappingURL=ducks.js.map