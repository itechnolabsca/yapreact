import { Epic } from 'redux-observable';
import { AnyAction } from 'redux';
import { ChatBoxState } from '../ChatBox/ducks';
declare type State = {
    chatBox: ChatBoxState;
};
declare const _default: Epic<AnyAction, State, any, AnyAction>;
export default _default;
