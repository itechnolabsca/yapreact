export interface ChatWallUser {
    id: string;
    thumb?: string;
    onlineStatus: boolean;
    name: string;
    unreadMessagesCount: number;
}
export interface ChatWallState {
    users: ChatWallUser[];
}
export declare const changeUserOnlineStatus: import("../../utils/createAction").YapActionCreator<{}>;
export declare const incUserMsgCount: import("../../utils/createAction").YapActionCreator<{}>;
export declare const resetUnreadMsgCount: import("../../utils/createAction").YapActionCreator<{}>;
export declare const pushUserToFront: import("../../utils/createAction").YapActionCreator<{}>;
export declare const updateUsers: import("../../utils/createAction").YapActionCreator<{}>;
declare const _default: import("redux").Reducer<ChatWallState>;
export default _default;
