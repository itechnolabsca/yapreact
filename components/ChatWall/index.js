"use strict";
exports.__esModule = true;
var React = require("react");
var react_redux_1 = require("react-redux");
var ChatUserTile_1 = require("../ChatUserTile");
var ChatBox_1 = require("../ChatBox");
var ducks_1 = require("../ChatBox/ducks");
var ChatWall = function (props) { return (React.createElement("div", null,
    React.createElement("h1", { className: "dashboard-title hidden-md-up" }, "ChatWall"),
    React.createElement("section", { className: "row text-center customers" }, props.users.map(function (user) { return (React.createElement(ChatUserTile_1["default"], { id: user.id, key: user.id, thumb: user.thumb, onlineStatus: user.onlineStatus, name: user.name, unreadMessagesCount: user.unreadMessagesCount, onTileClick: function () { return props.toggleChatWindow(user); } })); })),
    React.createElement(ChatBox_1["default"], null))); };
var mapStateToProps = function (_a) {
    var users = _a.chatWall.users;
    return ({
        users: users
    });
};
var mapDispatchToProps = function (dispatch) { return ({
    toggleChatWindow: function (user) { return dispatch(ducks_1.toggleChatWindow(user)); }
}); };
exports["default"] = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(ChatWall);
//# sourceMappingURL=index.js.map