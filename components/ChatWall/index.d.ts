/// <reference types="react-redux" />
import * as React from 'react';
import { YapAction } from '../../utils/createAction';
import { ChatWallUser } from './ducks';
export interface ChatWallProps {
    users: ChatWallUser[];
    toggleChatWindow: (user: ChatWallUser) => void;
}
declare const _default: React.ComponentClass<Pick<{
    users: ChatWallUser[];
} & {
    toggleChatWindow: (user: ChatWallUser) => YapAction<{}>;
}, never>> & {
    WrappedComponent: React.ComponentType<{
        users: ChatWallUser[];
    } & {
        toggleChatWindow: (user: ChatWallUser) => YapAction<{}>;
    }>;
};
export default _default;
