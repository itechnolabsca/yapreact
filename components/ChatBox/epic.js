"use strict";
exports.__esModule = true;
var redux_observable_1 = require("redux-observable");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/operator/do");
require("rxjs/add/operator/throttleTime");
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/delay");
require("rxjs/add/observable/from");
require("rxjs/add/observable/of");
var ducks_1 = require("./ducks");
var chatVisibilityEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$.ofType(ducks_1.toggleChatWindow().type).map(function (_a) {
        var user = _a.payload;
        var chatUser = getState().chatBox.user;
        var shallShow = !(chatUser && chatUser.id === user.id);
        return shallShow ? ducks_1.showChatWindow(user) : ducks_1.hideChatWindow(user);
    });
};
var requestPastMessagesEpic = function (socketman) { return function (action$) {
    return action$.ofType(ducks_1.showChatWindow().type).map(function (_a) {
        var user = _a.payload;
        socketman.emit('getWebMessages', {
            roomId: user.roomId,
            lastMessageId: ''
        });
        return { type: 'REQUESTED_PAST_MESSAGES', user: user };
    });
}; };
var acceptPastMessagesEpic = function (action$) {
    return action$.ofType('@@socketman/PAST_MESSAGES').map(function (_a) {
        var payload = _a.payload;
        var messages = payload.sort(function (m1, m2) { return m1.sendTime - m2.sendTime; });
        return ducks_1.saveUserMessages(messages);
    });
};
var receiveNewMessageEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType('@@socketman/NEW_MESSAGE')
        .mergeMap(function (_a) {
        var payload = _a.payload;
        if (Array.isArray(payload)) {
            return Observable_1.Observable.from(payload);
        }
        return Observable_1.Observable.of(payload);
    })
        .filter(function (payload) {
        var user = getState().chatBox.user;
        return user && payload.senderUserId === user.id;
    })
        .map(function (payload) {
        return ducks_1.receiveNewMessage(payload);
    });
};
var acknowledgeMsgEpic = function (socketman) { return function (action$, store) {
    return action$
        .filter(function (_a) {
        var type = _a.type;
        return type === 'SAVE_USER_MESSAGES' || type === 'RECEIVE_NEW_MESSAGE';
    })
        .mergeMap(function (action) {
        return Observable_1.Observable.from(Array.isArray(action.payload) ? action.payload : [action.payload]);
    })
        .filter(function (message) { return message.readCount === 0 || message.deliveredCount === 0; })["do"](function (msg) {
        var user = store.getState().ui.user;
        var deliverMsg = {
            type: 'changeMessageStatus_1',
            status: 'deliver',
            messageId: msg._id,
            criteria: 'single',
            roomId: user.roomId
        };
        var readMsg = {
            type: 'changeMessageStatus_1',
            status: 'read',
            messageId: msg._id,
            criteria: 'single',
            roomId: user.roomId
        };
        if (!window.localStorage.getItem('stealthMode')) {
            socketman.emit('changeMessageStatus_1', deliverMsg);
            socketman.emit('changeMessageStatus_1', readMsg);
        }
    })
        .map(function (message) { return ({ type: 'ACK_RECEIVED_MESSAGE', message: message }); });
}; };
var sendMessageEpic = function (socketman) { return function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType(ducks_1.sendMessage.start().type)
        .filter(function () {
        var state = getState();
        var message = state.chatBox.unsentMessage;
        return Boolean(message.trim());
    })
        .mergeMap(function () {
        var state = getState();
        var admin = state.ui.user;
        var chatUser = state.chatBox.user;
        var message = state.chatBox.unsentMessage;
        var time = new Date().getTime();
        var socketMsg = {
            roomId: chatUser.roomId,
            message: message,
            type: 'text',
            senderPhotoId: chatUser.id,
            senderUserId: admin._id,
            sendTime: time,
            localId: String(time),
            uniqueId: time
        };
        socketman.emit('newMessage', socketMsg);
        return Observable_1.Observable.from([ducks_1.changeUnsentMessage(''), ducks_1.addSentMessage(socketMsg)]);
    });
}; };
var removeFromQueueEpic = function (socketman) { return function (action$, _a) {
    var getState = _a.getState;
    return action$.ofType(ducks_1.removeFromQueue.start().type).map(function () {
        var state = getState();
        var admin = state.ui.user;
        var chatUser = state.chatBox.user;
        var socketMsg = {
            roomId: chatUser.roomId,
            senderPhotoId: chatUser.id,
            senderUserId: admin._id
        };
        console.log('remove form queue', socketMsg);
        socketman.emit('removeFromQueue', socketMsg);
        return ducks_1.removeFromQueue.success();
    });
}; };
var ackSentMsgEpic = function (action$) {
    return action$.ofType('@@socketman/ACK_SENT_MESSAGE').map(function (_a) {
        var payload = _a.payload;
        return ducks_1.ackSentMsg(payload[0]);
    });
};
var startTypingEpic = function (socketman) { return function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType('CHANGE_UNSENT_CHAT_MESSAGE')
        .throttleTime(1000)
        .map(function () {
        var state = getState();
        var chatUser = state.chatBox.user;
        var socketMsg = {
            roomId: chatUser.roomId,
            type: 'typing',
            status: '1'
        };
        socketman.emit('typing', socketMsg);
        return { type: 'SENT_START_TYPING_STATUS' };
    });
}; };
var stopTypingEpic = function (socketman) { return function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType('CHANGE_UNSENT_CHAT_MESSAGE')
        .debounceTime(2000)
        .map(function () {
        var state = getState();
        var chatUser = state.chatBox.user;
        var socketMsg = {
            roomId: chatUser.roomId,
            type: 'typing',
            status: '0'
        };
        socketman.emit('typing', socketMsg);
        return { type: 'SENT_STOP_TYPING_STATUS' };
    });
}; };
var scrollChatboxEpic = function (action$) { return action$.ofType().map(ducks_1.scrollToBottom); };
var incUnreadCountEpic = function (action$, _a) {
    var getState = _a.getState;
    return action$
        .ofType('RECEIVE_NEW_MESSAGE')
        .filter(function () { return getState().chatBox.hasScrolledUp; })
        .map(ducks_1.incUnreadCount);
};
var resetScrollToBottomEpic = function (action$) {
    return action$
        .ofType('CHATBOX_SCROLL_TO_BOTTOM')
        .delay(200)
        .map(ducks_1.resetScrollToBottom);
};
exports["default"] = (function (socketman) {
    return redux_observable_1.combineEpics(chatVisibilityEpic, requestPastMessagesEpic(socketman), acceptPastMessagesEpic, receiveNewMessageEpic, acknowledgeMsgEpic(socketman), sendMessageEpic(socketman), ackSentMsgEpic, startTypingEpic(socketman), stopTypingEpic(socketman), scrollChatboxEpic, incUnreadCountEpic, resetScrollToBottomEpic, removeFromQueueEpic(socketman));
});
//# sourceMappingURL=epic.js.map