"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var _a;
var utils_1 = require("../../utils");
var sendMessageDuck = utils_1.createDuck('SEND_CHAT_MESSAGE');
var removeFromQueueDuck = utils_1.createDuck('REMOVE_FROM_QUEUE');
var initialState = {
    user: null,
    messages: [],
    unsentMessage: '',
    newMessageCount: 0,
    shallScrollToBottom: false,
    hasScrolledUp: false
};
var ducks = { messageSendStatus: sendMessageDuck };
exports.toggleChatWindow = utils_1.createAction('TOGGLE_CHAT_WINDOW');
exports.changeUnsentMessage = utils_1.createAction('CHANGE_UNSENT_CHAT_MESSAGE');
exports.saveUserMessages = utils_1.createAction('SAVE_USER_MESSAGES');
exports.receiveNewMessage = utils_1.createAction('RECEIVE_NEW_MESSAGE');
exports.addSentMessage = utils_1.createAction('ADD_SENT_MESSAGE');
exports.ackSentMsg = utils_1.createAction('ACK_SENT_MESSAGE');
exports.scrollToBottom = utils_1.createAction('CHATBOX_SCROLL_TO_BOTTOM');
exports.resetScrollToBottom = utils_1.createAction('CHATBOX_RESET_SCROLL_TO_BOTTOM');
exports.scrollUp = utils_1.createAction('CHATBOX_SCROLL_UP');
exports.incUnreadCount = utils_1.createAction('CHATBOX_INC_UNREAD_COUNT');
exports.resetUnreadCount = utils_1.createAction('CHATBOX_RESET_UNREAD_COUNT');
exports.sendMessage = sendMessageDuck.actions;
exports.removeFromQueue = removeFromQueueDuck.actions;
exports.showChatWindow = utils_1.createAction('SHOW_CHAT_WINDOW');
exports.hideChatWindow = utils_1.createAction('HIDE_CHAT_WINDOW');
exports["default"] = utils_1.createReducer((_a = {},
    _a[exports.showChatWindow().type] = function (state, _a) {
        var user = _a.payload;
        return (__assign({}, state, { user: user }));
    },
    _a[exports.hideChatWindow().type] = function (state) { return (__assign({}, state, { user: null })); },
    _a.CHANGE_UNSENT_CHAT_MESSAGE = function (state, _a) {
        var message = _a.payload;
        return (__assign({}, state, { unsentMessage: message }));
    },
    _a.SAVE_USER_MESSAGES = function (state, _a) {
        var messages = _a.payload;
        return (__assign({}, state, { messages: messages }));
    },
    _a.RECEIVE_NEW_MESSAGE = function (state, _a) {
        var message = _a.payload;
        return (__assign({}, state, { messages: state.messages.concat([message]) }));
    },
    _a.ADD_SENT_MESSAGE = function (state, _a) {
        var message = _a.payload;
        return (__assign({}, state, { messages: state.messages.concat([message]) }));
    },
    _a.ACK_SENT_MESSAGE = function (state, _a) {
        var message = _a.payload;
        var newMessages = state.messages.map(function (oldMsg) {
            if (oldMsg.localId && oldMsg.localId === message.localId) {
                return message;
            }
            return oldMsg;
        });
        return __assign({}, state, { messages: newMessages });
    },
    _a.CHATBOX_SCROLL_UP = function (state) { return (__assign({}, state, { hasScrolledUp: true })); },
    _a.CHATBOX_SCROLL_TO_BOTTOM = function (state) { return (__assign({}, state, { shallScrollToBottom: true, hasScrolledUp: false, newMessageCount: 0 })); },
    _a.CHATBOX_INC_UNREAD_COUNT = function (state) { return (__assign({}, state, { newMessageCount: state.newMessageCount + 1 })); },
    _a.CHATBOX_RESET_UNREAD_COUNT = function (state) { return (__assign({}, state, { newMessageCount: 0 })); },
    _a.CHATBOX_RESET_SCROLL_TO_BOTTOM = function (state) { return (__assign({}, state, { shallScrollToBottom: false })); },
    _a), { initialState: initialState, ducks: ducks });
//# sourceMappingURL=ducks.js.map