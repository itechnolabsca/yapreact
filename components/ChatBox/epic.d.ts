import { Epic } from 'redux-observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import Socketman from 'socketman';
import { ChatBoxState } from './ducks';
import { AnyAction } from 'redux';
declare type UIUser = {
    _id: string;
    roomId?: string;
};
declare type State = {
    chatBox: ChatBoxState;
    ui: {
        user: UIUser;
    };
};
declare const _default: (socketman: Socketman) => Epic<AnyAction, State, any, AnyAction>;
export default _default;
