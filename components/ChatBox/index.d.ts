/// <reference types="react-redux" />
import * as React from 'react';
import { YapAction } from '../../utils/createAction';
import { ChatBoxMessage } from './ducks';
export interface ChatBoxUser {
    id: string;
    name: string;
    thumb?: string;
}
export interface ChatBoxProps {
    user: ChatBoxUser;
    placeholderThumb: string;
    messages: Array<ChatBoxMessage>;
    hasScrolledUp: boolean;
    unsentMessage: string;
    newMessageCount: number;
    shallScrollToBottom: boolean;
    onCloseChat: (u: ChatBoxUser) => (e: any) => void;
    onSendMessage: () => void;
    removeFromQueue: () => void;
    onScrollUp: () => void;
    resetUnreadCount: () => void;
    onChangeMessage: (m: string) => void;
}
declare const _default: React.ComponentClass<Pick<{
    user: any;
    messages: ChatBoxMessage[];
    unsentMessage: string;
    newMessageCount: number;
    shallScrollToBottom: boolean;
    hasScrolledUp: boolean;
} & {
    onCloseChat: (user: ChatBoxUser) => () => YapAction<{}>;
    removeFromQueue: () => YapAction<{}>;
    onSendMessage: () => YapAction<{}>;
    onChangeMessage: (msg: string) => YapAction<{}>;
    onScrollUp: () => YapAction<{}>;
    resetUnreadCount: () => YapAction<{}>;
}, never>> & {
    WrappedComponent: React.ComponentType<{
        user: any;
        messages: ChatBoxMessage[];
        unsentMessage: string;
        newMessageCount: number;
        shallScrollToBottom: boolean;
        hasScrolledUp: boolean;
    } & {
        onCloseChat: (user: ChatBoxUser) => () => YapAction<{}>;
        removeFromQueue: () => YapAction<{}>;
        onSendMessage: () => YapAction<{}>;
        onChangeMessage: (msg: string) => YapAction<{}>;
        onScrollUp: () => YapAction<{}>;
        resetUnreadCount: () => YapAction<{}>;
    }>;
};
export default _default;
