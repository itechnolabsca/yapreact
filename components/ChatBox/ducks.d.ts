import { YapActionCreator } from '../../utils/createAction';
import { YapAsyncAction } from '../../utils/createActionAsync';
export interface ChatBoxMessage {
    localId: string;
    uniqueId: string;
    senderUserId?: string;
    type: string;
    message: string;
    deliveredCount: number;
    readCount: number;
    sendTime: any;
}
export interface ChatBoxState {
    user: any;
    messages: ChatBoxMessage[];
    unsentMessage: string;
    newMessageCount: number;
    shallScrollToBottom: boolean;
    hasScrolledUp: boolean;
}
export declare const toggleChatWindow: YapActionCreator<{}>;
export declare const changeUnsentMessage: YapActionCreator<{}>;
export declare const saveUserMessages: YapActionCreator<{}>;
export declare const receiveNewMessage: YapActionCreator<{}>;
export declare const addSentMessage: YapActionCreator<{}>;
export declare const ackSentMsg: YapActionCreator<{}>;
export declare const scrollToBottom: YapActionCreator<{}>;
export declare const resetScrollToBottom: YapActionCreator<{}>;
export declare const scrollUp: YapActionCreator<{}>;
export declare const incUnreadCount: YapActionCreator<{}>;
export declare const resetUnreadCount: YapActionCreator<{}>;
export declare const sendMessage: YapAsyncAction;
export declare const removeFromQueue: YapAsyncAction;
export declare const showChatWindow: YapActionCreator<{}>;
export declare const hideChatWindow: YapActionCreator<{}>;
declare const _default: import("redux").Reducer<ChatBoxState>;
export default _default;
