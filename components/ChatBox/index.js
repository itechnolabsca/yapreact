"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var ducks_1 = require("./ducks");
var ChatMessage_1 = require("../ChatMessage");
var ChatBox = /** @class */ (function (_super) {
    __extends(ChatBox, _super);
    function ChatBox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollToBottom = function () {
            // do not start an interval if not needed
            var interval = setInterval(function () {
                if (_this.chatBoxRef && _this.chatBoxRef.lastElementChild) {
                    clearInterval(interval);
                    _this.chatBoxRef.lastElementChild.scrollIntoView();
                }
            }, 100);
        };
        _this.handleChatScroll = function () {
            if (!_this.scrollRef) {
                return;
            }
            // Magic number 380
            var scrollDistance = _this.scrollRef.scrollHeight - _this.scrollRef.scrollTop - 380;
            if (scrollDistance > 100 && !_this.props.hasScrolledUp) {
                _this.props.onScrollUp();
            }
            if (scrollDistance < 10 && _this.props.hasScrolledUp) {
                _this.props.resetUnreadCount();
            }
        };
        _this.messagesJsx = function () {
            var _a = _this.props, messages = _a.messages, user = _a.user;
            return messages.map(function (message) { return (React.createElement(ChatMessage_1["default"], { key: message.uniqueId, message: message, senderId: message.senderUserId, receiverId: user.id })); });
        };
        _this.unreadBadgeJsx = function (count) {
            return (React.createElement("div", { className: "chatScreenNotification", onClick: _this.scrollToBottom },
                React.createElement("i", { className: "fa fa-angle-down", "aria-hidden": "true" }),
                React.createElement("span", { className: "badge" }, count)));
        };
        return _this;
    }
    ChatBox.prototype.componentWillUpdate = function (newProps) {
        if ((newProps.user && !this.props.user) ||
            (!newProps.user && this.props.user) ||
            (this.props.user && newProps.user.id !== this.props.user.id)) {
            this.scrollToBottom();
        }
    };
    ChatBox.prototype.componentDidUpdate = function () {
        if (this.props.shallScrollToBottom) {
            this.scrollToBottom();
        }
    };
    ChatBox.prototype.render = function () {
        var _this = this;
        var _a = this.props, user = _a.user, messages = _a.messages, unsentMessage = _a.unsentMessage, newMessageCount = _a.newMessageCount, onCloseChat = _a.onCloseChat, onSendMessage = _a.onSendMessage, removeFromQueue = _a.removeFromQueue, onChangeMessage = _a.onChangeMessage, placeholderThumb = _a.placeholderThumb;
        return (user && (React.createElement("div", { className: "chatView" },
            React.createElement("div", { className: "scroll_body" },
                React.createElement("div", { className: "message_title" },
                    React.createElement("span", { className: "chatProfilePic" },
                        React.createElement("img", { src: user.thumb || placeholderThumb, alt: user.name })),
                    user.name,
                    React.createElement("i", { className: "fa fa-check-circle removeFromQueue", onClick: removeFromQueue }, "\u00A0"),
                    React.createElement("i", { className: "fa fa-times close-btn", onClick: onCloseChat(user) }, "\u00A0")),
                React.createElement("div", { className: "message_body", ref: function (ref) { return (_this.scrollRef = ref); }, onScroll: this.handleChatScroll },
                    React.createElement("ul", { id: "messages", ref: function (ref) { return (_this.chatBoxRef = ref); } }, this.messagesJsx())),
                React.createElement("div", { className: "messageTrigger" },
                    newMessageCount !== 0 && this.unreadBadgeJsx(newMessageCount),
                    React.createElement("i", { className: "fa fa-paper-plane send", onClick: function () {
                            onSendMessage();
                            _this.scrollToBottom();
                        } }, "\u00A0"),
                    React.createElement("input", { type: "text", value: unsentMessage, onChange: function (event) { return onChangeMessage(event.target.value); }, onKeyPress: function (event) {
                            if (event.key === 'Enter') {
                                onSendMessage();
                                _this.scrollToBottom();
                            }
                        } }))))));
    };
    return ChatBox;
}(react_1.Component));
var mapStateToProps = function (_a) {
    var _b = _a.chatBox, user = _b.user, messages = _b.messages, unsentMessage = _b.unsentMessage, newMessageCount = _b.newMessageCount, hasScrolledUp = _b.hasScrolledUp, shallScrollToBottom = _b.shallScrollToBottom;
    return ({
        user: user,
        messages: messages,
        unsentMessage: unsentMessage,
        newMessageCount: newMessageCount,
        shallScrollToBottom: shallScrollToBottom,
        hasScrolledUp: hasScrolledUp
    });
};
var mapDispatchToProps = function (dispatch) { return ({
    onCloseChat: function (user) { return function () { return dispatch(ducks_1.toggleChatWindow(user)); }; },
    removeFromQueue: function () { return dispatch(ducks_1.removeFromQueue.start()); },
    onSendMessage: function () { return dispatch(ducks_1.sendMessage.start()); },
    onChangeMessage: function (msg) { return dispatch(ducks_1.changeUnsentMessage(msg)); },
    onScrollUp: function () { return dispatch(ducks_1.scrollUp()); },
    resetUnreadCount: function () { return dispatch(ducks_1.resetUnreadCount()); }
}); };
// HACK: FIXME: ChatBox as any
exports["default"] = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(ChatBox);
//# sourceMappingURL=index.js.map