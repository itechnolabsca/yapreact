"use strict";
exports.__esModule = true;
var React = require("react");
var classnames = require("classnames");
var messageParser_1 = require("./messageParser");
var formatTime = function (time) {
    var date = new Date(time);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var minutes = String(date.getMinutes());
    var hours = date.getHours();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours %= 12;
    // eslint-disable-next-line
    hours = hours ? hours : 12;
    minutes = parseInt(minutes, 10) < 10 ? "0" + minutes : minutes;
    var strTime = day + "/" + month + "/" + year + " " + hours + ":" + minutes + " " + ampm;
    return strTime;
};
var Message = function (_a) {
    var message = _a.message;
    switch (message.type) {
        case 'text':
            return React.createElement("span", null, message.value);
        case 'image':
            // eslint-disable-next-line jsx-a11y/img-redundant-alt
            return React.createElement("img", { src: message.value, alt: "Failed to load Image message" });
        case 'nl':
            return React.createElement("br", null);
        case 'link':
            return React.createElement("a", { href: message.value.href }, message.value.name);
        case 'location':
            return (React.createElement("a", { href: "https://www.google.com/maps?q=" + message.value.latitude + "," + message.value.longitude, target: "_blank" },
                React.createElement("img", { src: "https://maps.google.com/maps/api/staticmap?center=" + message.value.latitude + "," + message.value.longitude + "&zoom=14&size=300x150&&markers=color:red%7Clabel:C%7C" + message.value.latitude + "," + message.value.longitude + "&key=AIzaSyBZLMfSbimJP-GhdZNQ3nwZzyYDl0DQJhA", alt: message.value.name })));
        case 'video':
            return React.createElement("video", { src: message.value, height: 190, width: 190, controls: true });
        default:
            throw new Error('Unknown Message');
    }
};
var ChatMessage = function (_a) {
    var message = _a.message, senderId = _a.senderId, receiverId = _a.receiverId;
    return (React.createElement("li", { className: classnames('message ', receiverId === senderId && 'messageReceived') },
        messageParser_1["default"](message).map(function (msg, index) { return React.createElement(Message, { key: index, message: msg }); }),
        React.createElement("i", { className: classnames(receiverId === senderId ? 'messageReceived' : 'messagesent', message.deliveredCount && 'messagedelivered', message.readCount && 'messageread') },
            formatTime(message.sendTime),
            " ",
            receiverId !== senderId && React.createElement("span", null))));
};
exports["default"] = ChatMessage;
//# sourceMappingURL=index.js.map