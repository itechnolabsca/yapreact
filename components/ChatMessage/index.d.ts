/// <reference types="react" />
import { ChatMessage } from './messageParser';
export interface ChatMessageInput {
    message: {
        type: string;
        message: string;
        deliveredCount: number;
        readCount: number;
        sendTime: any;
    };
    senderId: string;
    receiverId: string;
}
declare const ChatMessage: ({ message, senderId, receiverId }: ChatMessageInput) => JSX.Element;
export default ChatMessage;
