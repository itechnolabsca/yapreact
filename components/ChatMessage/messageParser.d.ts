export interface NewLineMessage {
    type: 'nl';
    value: null;
}
export interface TextMessage {
    type: 'text';
    value: string;
}
export interface ImageMessage {
    type: 'image';
    value: string;
}
export interface VideoMessage {
    type: 'video';
    value: string;
}
export interface LinkMessage {
    type: 'link';
    value: {
        name: string;
        href: string;
    };
}
export interface LocationMessage {
    type: 'location';
    value: {
        name: string;
        latitude: number;
        longitude: number;
    };
}
export declare type ChatMessage = NewLineMessage | TextMessage | ImageMessage | VideoMessage | LinkMessage | LocationMessage;
export interface Tokenizable {
    message: string;
    type: 'text' | 'imageText' | 'gifText' | 'locationText' | 'shareLink' | 'videoText' | string;
}
/**
 * Returns array of basic message units.
 * Each array item should become a single message, with its content message units shown in a
 * single message bubble
 */
declare const tokenize: ({ message, type }: Tokenizable) => ChatMessage[];
export default tokenize;
