"use strict";
exports.__esModule = true;
// Message that come from server is semi-parsed already. But we parse it further
// into basic units that we finally convert to JSX
var createLogger_1 = require("../../utils/createLogger");
var log = createLogger_1["default"]('yapreact:MessageParser');
var tokenizeText = function (text) {
    var breakRegex = /\n|(https?:\/\/[^\s]+)/gi;
    var urlRegex = /(https?:\/\/[^\s]+)/gi;
    // Gotta remove empty strings. Newlines show up as 'undefined'
    var tokens = text.split(breakRegex).filter(function (tok) { return tok !== ''; });
    return tokens.map(function (token) {
        if (token === undefined) {
            return { type: 'nl', value: null };
        }
        if (urlRegex.test(token)) {
            return {
                type: 'link',
                value: {
                    name: token,
                    href: token
                }
            };
        }
        return {
            type: 'text',
            value: token
        };
    });
};
/**
 * Returns array of basic message units.
 * Each array item should become a single message, with its content message units shown in a
 * single message bubble
 */
var tokenize = function (_a) {
    var message = _a.message, type = _a.type;
    var messageJson;
    switch (type) {
        case 'text':
            return tokenizeText(message);
        case 'imageText':
            return [
                {
                    type: 'image',
                    value: message
                }
            ];
        case 'gifText':
            return [
                {
                    type: 'image',
                    value: message
                }
            ];
        case 'locationText':
            messageJson = JSON.parse(message);
            return [
                {
                    type: 'location',
                    value: {
                        name: messageJson.name,
                        latitude: messageJson.latitude,
                        longitude: messageJson.longitude
                    }
                }
            ];
        case 'shareLink':
            messageJson = JSON.parse(message);
            return messageJson.shareLinkArray.reduce(function (accum, link) {
                accum.push({
                    type: 'image',
                    value: link.imageLink
                });
                accum.push({
                    type: 'link',
                    value: {
                        name: link.text,
                        href: link.text
                    }
                });
                return accum;
            }, []);
        case 'videoText':
            return [
                {
                    type: 'video',
                    value: message
                }
            ];
        default:
            log('Received unknown message', message, 'of type', type);
            return [{ type: 'text', value: message }];
    }
};
exports["default"] = tokenize;
//# sourceMappingURL=messageParser.js.map