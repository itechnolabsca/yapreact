"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var React = require("react");
var DayPickerInput_1 = require("react-day-picker/DayPickerInput");
require("./style.css");
var DayPickerField = function (_a) {
    var input = _a.input, _b = _a.meta, touched = _b.touched, error = _b.error, wrapperClassName = _a.wrapperClassName, overlayClassName = _a.overlayClassName, inputClassName = _a.inputClassName, errorClassName = _a.errorClassName, placeholder = _a.placeholder, dayPickerProps = _a.dayPickerProps;
    var wrapperClass = wrapperClassName || 'form-group';
    var inputClass = inputClassName || 'DayPickerInput';
    var overlayClass = overlayClassName || 'DayPickerInput-Overlay';
    var errorClass = errorClassName || 'error text-danger';
    dayPickerProps = dayPickerProps || {};
    return (React.createElement("div", { className: wrapperClass },
        React.createElement(DayPickerInput_1["default"], __assign({ placeholder: placeholder, value: input.value, onDayChange: input.onChange, classNames: { container: inputClass, overlay: overlayClass } }, dayPickerProps)),
        touched && (error && React.createElement("div", { className: errorClass }, error))));
};
exports["default"] = DayPickerField;
//# sourceMappingURL=index.js.map