/// <reference types="react" />
import './style.css';
export interface DaypickerFieldProps {
    input: {
        value: string;
        onChange: () => void;
        [key: string]: any;
    };
    meta: {
        touched: boolean;
        error: string;
    };
    wrapperClassName?: string;
    inputClassName?: string;
    errorClassName?: string;
    overlayClassName?: string;
    placeholder?: string;
    dayPickerProps?: any;
}
declare const DayPickerField: ({ input, meta: { touched, error }, wrapperClassName, overlayClassName, inputClassName, errorClassName, placeholder, dayPickerProps }: DaypickerFieldProps) => JSX.Element;
export default DayPickerField;
