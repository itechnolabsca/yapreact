"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var React = require("react");
var classnames = require("classnames");
var InputField = function (_a) {
    var input = _a.input, type = _a.type, pattern = _a.pattern, placeholder = _a.placeholder, label = _a.label, autoComplete = _a.autoComplete, wrapperClassName = _a.wrapperClassName, inputClassName = _a.inputClassName, errorClassName = _a.errorClassName, _b = _a.meta, touched = _b.touched, error = _b.error;
    var _c;
    var onChange = function (e) {
        var val = e.target.value;
        if (val && pattern && !pattern.test(val)) {
            return;
        }
        input.onChange(e);
    };
    var showError = touched && error;
    var inputClass = inputClassName || "form-control";
    var errorClass = errorClassName || "error text-danger";
    return (React.createElement("div", { className: classnames((_c = {},
            _c[wrapperClassName] = wrapperClassName,
            _c["form-group"] = !wrapperClassName,
            _c["has-error"] = showError,
            _c)) },
        label !== null &&
            input.value && React.createElement("div", { className: "label" }, label || placeholder),
        type === "checkbox" && (React.createElement("span", { className: classnames("checkbox-before", {
                "checkbox-before-checked": input.checked
            }) })),
        React.createElement("input", __assign({}, input, { onChange: onChange, type: type, className: inputClass, placeholder: placeholder, autoComplete: autoComplete, value: input.value })),
        showError && React.createElement("div", { className: errorClassName }, error)));
};
exports["default"] = InputField;
//# sourceMappingURL=index.js.map