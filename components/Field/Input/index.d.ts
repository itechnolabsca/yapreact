/// <reference types="react" />
export interface InputFieldProps {
    input: {
        onChange: (e: any) => void;
        value?: string;
        [key: string]: any;
    };
    type?: string;
    pattern?: RegExp;
    placeholder?: string;
    label?: string;
    autoComplete?: string;
    wrapperClassName?: string;
    inputClassName?: string;
    errorClassName?: string;
    meta: {
        touched: boolean;
        error: string;
    };
}
declare const InputField: ({ input, type, pattern, placeholder, label, autoComplete, wrapperClassName, inputClassName, errorClassName, meta: { touched, error } }: InputFieldProps) => JSX.Element;
export default InputField;
