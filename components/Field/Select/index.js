"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var React = require("react");
var SelectField = function (_a) {
    var input = _a.input, label = _a.label, type = _a.type, _b = _a.meta, touched = _b.touched, error = _b.error, children = _a.children, placeholder = _a.placeholder, wrapperClassName = _a.wrapperClassName, inputClassName = _a.inputClassName, errorClassName = _a.errorClassName;
    var wrapperClass = wrapperClassName || 'form-group';
    var inputClass = inputClassName || 'form-control';
    var errorClass = errorClassName || 'error text-danger';
    return (React.createElement("div", { className: wrapperClass },
        input.value && React.createElement("div", { className: "label" }, label || placeholder),
        React.createElement("select", __assign({ className: inputClass }, input), children),
        touched && (error && React.createElement("div", { className: errorClass }, error))));
};
exports["default"] = SelectField;
//# sourceMappingURL=index.js.map