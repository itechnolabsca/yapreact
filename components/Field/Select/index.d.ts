import { ReactElement } from 'react';
import { Options } from 'react-redux';
export interface SelectFieldProps {
    input: {
        value?: string;
        [key: string]: any;
    };
    type?: string;
    pattern?: RegExp;
    placeholder?: string;
    label?: string;
    meta: {
        touched: boolean;
        error: string;
    };
    children?: Array<ReactElement<Options>>;
    wrapperClassName?: string;
    inputClassName?: string;
    errorClassName?: string;
}
declare const SelectField: ({ input, label, type, meta: { touched, error }, children, placeholder, wrapperClassName, inputClassName, errorClassName }: SelectFieldProps) => JSX.Element;
export default SelectField;
