"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_1 = require("react");
var classnames = require("classnames");
var createLogger_1 = require("../../../utils/createLogger");
var UploadImage_1 = require("../../UploadImage");
var log = createLogger_1["default"]('UploadImageField');
var UploadImageField = /** @class */ (function (_super) {
    __extends(UploadImageField, _super);
    function UploadImageField() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            error: '',
            isUploading: false,
            isShowingPreview: false,
            isFocused: false
        };
        _this.setupPreview = function (image) {
            return _this.state.isShowingPreview && (React.createElement("div", { className: "mega-image-viewer" },
                React.createElement("div", { className: "mega-image" },
                    React.createElement("img", { src: image, alt: "" }),
                    React.createElement("span", { className: "fa fa-close", onClick: function () {
                            _this.setState({ isShowingPreview: false });
                        } }))));
        };
        _this.handleProgress = function (progress) {
            var loadingEl = _this.loadingEl;
            _this.setState({ isUploading: true, error: '' });
            if (!loadingEl) {
                log('Loading Element not found', loadingEl); // eslint-disable-line no-console
            }
            else {
                loadingEl.style.width = progress + "%";
            }
            _this.props.onProgress(progress);
        };
        _this.handleError = function (error) {
            _this.setState({ error: error });
            _this.props.onError(error);
        };
        _this.handleDoneUpload = function (image) {
            _this.setState({
                isUploading: false,
                error: ''
            });
            _this.props.onDoneUpload(image);
        };
        return _this;
    }
    UploadImageField.prototype.render = function () {
        var _this = this;
        var _a = this.props, input = _a.input, label = _a.label, successLabel = _a.successLabel, noPreview = _a.noPreview, _b = _a.meta, touched = _b.touched, formError = _b.error, value = _b.value, initial = _b.initial, warning = _b.warning;
        var _c = this.state, uploadError = _c.error, isUploading = _c.isUploading, isFocused = _c.isFocused;
        var image = input.value || value || initial || '';
        var hasImage = !isUploading && image && image.length;
        var ext = hasImage &&
            image
                .split('.')
                .pop()
                .toLowerCase();
        var shallShowPreview = !noPreview && this.props.previewableFileFormats.includes(ext);
        var error = uploadError || formError;
        var hasError = uploadError || (touched && error);
        if (!image && !isUploading && this.loadingEl) {
            this.loadingEl.style.width = '0';
        }
        return (React.createElement("div", { className: "form-group" },
            this.setupPreview(image),
            React.createElement("div", { className: classnames({
                    'upload-control': true,
                    'has-image-preview': hasImage && shallShowPreview,
                    'has-error': hasError,
                    'is-focused': isFocused
                }) },
                shallShowPreview &&
                    hasImage && (React.createElement("img", { className: "uploaded-image-preview", src: image, onClick: function () {
                        _this.setState({ isShowingPreview: true });
                    }, alt: "Preview" })),
                React.createElement(UploadImage_1["default"], { className: "form-control", onProgress: this.handleProgress, onError: this.handleError, onDoneUpload: this.handleDoneUpload, onChange: input.onChange, s3Url: this.props.s3Url, value: image, onFocus: function () { return _this.setState({ isFocused: true }); }, onBlur: function () { return _this.setState({ isFocused: false }); }, supportedFileFormats: this.props.supportedFileFormats }),
                React.createElement("span", null,
                    React.createElement("i", { className: "fa fa-cloud-upload" }, "\u00A0"),
                    !hasImage && (isUploading ? 'Uploading...' : label || 'Click to Upload'),
                    hasImage && !isUploading && React.createElement("i", null, successLabel || 'Image Uploaded')),
                React.createElement("i", { className: "loading", ref: function (ref) {
                        _this.loadingEl = ref;
                    } }, "\u00A0"),
                React.createElement("i", { className: "available-formats" },
                    "Supported Formats:",
                    ' ',
                    this.props.supportedFileFormats.map(function (s) { return s.toUpperCase(); }).join(', ')),
                hasError && React.createElement("div", { className: "error text-danger signup-upload-error" }, error),
                touched &&
                    (warning && (React.createElement("div", { className: "warning text-warning signup-upload-warning" }, warning))))));
    };
    UploadImageField.defaultProps = {
        noPreview: false,
        onDoneUpload: function () { return ({}); },
        onChange: function () { return ({}); },
        onError: function () { return ({}); },
        onProgress: function () { return ({}); },
        supportedFileFormats: ['png', 'jpeg', 'jpg', 'csv', 'pdf'],
        previewableFileFormats: ['png', 'jpeg', 'jpg']
    };
    return UploadImageField;
}(react_1.Component));
/* eslint-enable */
exports["default"] = UploadImageField;
//# sourceMappingURL=index.js.map