import { Component } from 'react';
export interface UploadImageFieldProps {
    formError?: string;
    value?: string;
    label?: string;
    s3Url: string;
    onDoneUpload?: (url: string) => void;
    onChange?: (url: string) => void;
    onError: (url: string) => void;
    onProgress: (progress: number) => void;
    successLabel: string;
    noPreview?: boolean;
    input: {
        onChange: (e: any) => void;
        value?: string;
        [key: string]: any;
    };
    meta: {
        touched: boolean;
        error: string;
        value?: string;
        initial: string;
        warning: string;
    };
    supportedFileFormats?: string[];
    previewableFileFormats?: string[];
}
declare class UploadImageField extends Component<UploadImageFieldProps> {
    static defaultProps: {
        noPreview: boolean;
        onDoneUpload: () => {};
        onChange: () => {};
        onError: () => {};
        onProgress: () => {};
        supportedFileFormats: string[];
        previewableFileFormats: string[];
    };
    loadingEl: HTMLElement;
    state: {
        error: string;
        isUploading: boolean;
        isShowingPreview: boolean;
        isFocused: boolean;
    };
    setupPreview: (image: string) => JSX.Element;
    handleProgress: (progress: number) => void;
    handleError: (error: string) => void;
    handleDoneUpload: (image: string) => void;
    render(): JSX.Element;
}
export default UploadImageField;
