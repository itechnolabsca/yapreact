"use strict";
exports.__esModule = true;
exports["default"] = (function (type) { return function (payload) { return ({
    type: type,
    payload: payload
}); }; });
//# sourceMappingURL=createAction.js.map