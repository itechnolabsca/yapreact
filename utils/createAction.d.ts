export interface YapAction<T> {
    type: string;
    payload?: T;
}
export declare type YapActionCreator<T> = (payload?: T) => YapAction<T>;
declare const _default: <T>(type: string) => YapActionCreator<T>;
export default _default;
