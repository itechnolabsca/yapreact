import * as debug from 'debug';
declare const logger: (name: string) => debug.IDebugger;
export default logger;
