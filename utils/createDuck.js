"use strict";
exports.__esModule = true;
var createActionAsync_1 = require("./createActionAsync");
exports["default"] = (function (name, initialData, options) {
    var data = initialData === undefined ? [] : initialData;
    options = options || {};
    var storeNothing = data === null || options.storeNothing === true;
    var initialState = {
        isBusy: false,
        data: data,
        error: ''
    };
    var actions = createActionAsync_1["default"](name.toUpperCase());
    var reducerMap = function (stateKey) {
        var _a;
        return (_a = {},
            _a[actions.start().type] = function (state) {
                var _a;
                return Object.assign({}, state, (_a = {},
                    _a[stateKey] = {
                        isBusy: true,
                        error: '',
                        data: storeNothing
                            ? initialState.data
                            : options.clearOnStart === false ? state[stateKey].data : data
                    },
                    _a));
            },
            _a[actions.fail().type] = function (state, _a) {
                var payload = _a.payload;
                var _b;
                return Object.assign({}, state, (_b = {},
                    _b[stateKey] = {
                        isBusy: false,
                        error: payload,
                        data: storeNothing
                            ? initialState.data
                            : options.clearOnError === false ? state[stateKey].data : data
                    },
                    _b));
            },
            _a[actions.success().type] = function (state, _a) {
                var payload = _a.payload;
                var _b;
                return Object.assign({}, state, (_b = {},
                    _b[stateKey] = {
                        error: '',
                        isBusy: false,
                        data: storeNothing ? initialState.data : payload
                    },
                    _b));
            },
            _a);
    };
    return {
        actions: actions,
        initialState: initialState,
        reducerMap: reducerMap
    };
});
//# sourceMappingURL=createDuck.js.map