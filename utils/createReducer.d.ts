import { Reducer } from 'redux';
import { Duck } from './createDuck';
export interface ReducerMap<S> {
    [type: string]: Reducer<S>;
}
export interface ReducerOptions<S> {
    initialState?: S;
    ducks?: {
        [stateKey: string]: Duck<any, S>;
    };
}
declare const _default: <S>(reducers: ReducerMap<S>, options: ReducerOptions<S>) => Reducer<S>;
export default _default;
