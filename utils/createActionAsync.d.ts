import { YapAction } from './createAction';
export interface YapAsyncAction {
    start: <T>(payload?: T) => YapAction<T>;
    success: <T>(payload?: T) => YapAction<T>;
    fail: <T>(payload?: T) => YapAction<T>;
}
declare const _default: (type: string) => YapAsyncAction;
export default _default;
