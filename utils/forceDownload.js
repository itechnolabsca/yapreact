"use strict";
exports.__esModule = true;
exports["default"] = (function (href) {
    var link = document.createElement('a');
    link.href = href;
    link.download = href;
    link.target = '_blank';
    link.click();
});
//# sourceMappingURL=forceDownload.js.map