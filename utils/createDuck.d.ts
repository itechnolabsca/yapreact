import { YapAsyncAction } from './createActionAsync';
import { ReducerMap } from './createReducer';
export interface DuckOptions {
    clearOnStart?: boolean;
    clearOnError?: boolean;
    storeNothing?: boolean;
}
export interface DuckStateNode<T> {
    isBusy: boolean;
    error: string;
    data: T | any[];
}
export interface Duck<T, S = {}> {
    reducerMap: (type: string) => ReducerMap<S>;
    actions: YapAsyncAction;
    initialState: DuckStateNode<T>;
}
declare const _default: <T, S>(name: string, initialData?: any[] | T, options?: DuckOptions) => Duck<T, S>;
export default _default;
