"use strict";
exports.__esModule = true;
var debug = require("debug");
var logger = function (name) { return debug("YAP:" + name); };
exports["default"] = logger;
//# sourceMappingURL=createLogger.js.map