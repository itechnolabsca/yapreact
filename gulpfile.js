const gulp = require('gulp');
const del = require('del');

const resourcePaths = ['./src/**/*.?(s)css', './src/**/*.[png|svg|jpeg|jpg|gif]'];
const destPaths = ['./components', './utils'];

gulp.task('clean', () => del(destPaths));

gulp.task('default', ['clean'], () =>
  gulp.src(resourcePaths, { base: './src/' }).pipe(gulp.dest('./'))
);
